import React from 'react';
import md5 from 'md5';
import { Form, Icon, Input, Button, Checkbox, Spin } from 'antd';
import Background from '../img/bg2.jpg';
import logo from '../img/vcc_logo_144x144.png';
import { ajaxLogin,setCookie } from '../function/Api';


class Login extends React.Component {
  // login
  handleSubmit = e => {
    e.preventDefault();
    let Input = this.props.form.getFieldsValue();

    if (Input.username !== undefined && Input.password !== undefined) {
      ajaxLogin({
        userid: Input.username,
        password: md5(Input.password)
      });


    };

    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('成功啦: ', values);
    
      };
    });
  };
  
  
  render() {
    const { getFieldDecorator } = this.props.form;
    const logoSass = {
      width: '55px',
      margin: '0 auto 20px auto',
      display: 'block'
    };
    const loding = {
      textAlign: 'center',
      borderRadius: '4px',
      marginBottom: '20px',
      padding: '30px 50px',
      margin: '20px 0',
    }

    return (

      <div id="login-main" style={{ background: `url(${Background})no-repeat fixed bottom center`, backgroundSize: 'cover' }}>
        <Form onSubmit={this.handleSubmit} className="login-form">
          {Input.password !== undefined ? <div className="loding" style={loding}>
            <Spin size="large" />
          </div> : null}
          <img src={logo} alt="Logo" style={logoSass} />
          <h4 className="text-center">SMT 登入</h4>
          <Form.Item>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: '帳號錯誤!' }],
            })(
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="帳號"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: '密碼錯誤!' }],
            })(
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                type="password"
                placeholder="密碼"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('remember', {
              initialValue: false,
            })(<Checkbox>記住一週狀態</Checkbox>)}
          </Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button" block>
            登入
          </Button>

        </Form>
      </div>
    );
  };
};

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login);
export default WrappedNormalLoginForm;
