import React, { Component } from 'react';
import uuid from 'uuid/v4'
import {
    Row,
    Col,
    Form,
    Tabs,
    Input,
    DatePicker,
    Select,
    Button,
    Radio,

} from "antd";
import ContactForm from '../component/Contact';
import { searchDetail, ajaxApi, getMenu, getCarMenu, getCityAreaMenu, getCarDetailMenu, getEventMenu, getCarDetailCodeMenu, googleCheckLink, googleCancel, writeTab, getList } from "../function/Api";
import { convertDate, convertYear, success, setNextContactDate } from '../function/function';

import moment from "moment";
import "moment/locale/zh-cn";
import { relative } from "path";
moment.locale("zh-cn");

const time = new Date().getTime();
console.log(time)
const dateFormat = "YYYY/MM/DD";
const { TextArea } = Input;
const prefixSelector = (
    <Select style={{ width: 70 }}>
        <Select.Option value="M">手機</Select.Option>
        <Select.Option value="C">公司</Select.Option>
        <Select.Option value="H">住家</Select.Option>
    </Select>
);

class TabImformation extends Component {
    state = {
        TypeVal: '',
        editExistingBrand: [],
        editType: [],
        editEventInfo: [],
        editOccupation: [],
        editExistingBrand: [],
        contacts: [time],
        getCity: [],
        getEvent: [],
        renderImg: [],
        getArea: [],
        tabData: [],
        isopen: false,
        editCity: '',
        editCity2: '',
        validateStatus: '',
        contact: 1,
        address: '',
        address2: '',
    };
    async componentDidMount() {
        let editType = await getMenu("typ");
        let editEventInfo = await getMenu("ifo");
        let editOccupation = await getMenu("job");
        let editExistingBrand = await getMenu("brd");
        let tabData = await searchDetail();
        console.log(tabData)
        let getCity = await getCityAreaMenu("");
        let getEvent = await getEventMenu();
        let renderImg = await writeTab();
        await searchDetail();
        this.setState({
            tabData: tabData.data[0],
            renderImg: renderImg.filedata,
            getCity: getCity.data,
            editType: editType.data,
            editEventInfo: editEventInfo.data,
            editOccupation: editOccupation.data,
            editExistingBrand: editExistingBrand.data,
            validateStatus: "",
            selectedEventMenu: getEvent.data
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log('這邊是', values)

        });

        this.props.form.validateFields((err, values) => {
            console.log('這邊是', values)
            let formErr =
                !values.editPhoneType && !values.editEmail && !values.editLine;

            if (formErr) {
                this.setState({
                    validateStatus: "warning"
                });
            } else {
                this.setState({
                    validateStatus: ""
                });
            };
            if (!err) {
                console.log("Received values of form: ", values);
                this.editTab1Save();

            };
        });
    };

    addContact = () => {
        console.log('test')
        const newTime = new Date().getTime();
        this.setState({
            contacts: this.state.contacts.concat(newTime)
        });
    };

    removeContact = targetTime => {
        this.setState({
            contacts: this.state.contacts.filter(time => targetTime !== time)
        });
    };

    //save tab1
    editTab1Save = async () => {
        let values = this.props.form.getFieldsValue();
        let come_date = await convertDate(values.editComeDate._d);
        let editExistingYear, editEventDate, editBirthday;

        if (values.editBirthday !== undefined) {
            editBirthday = await convertDate(values.editBirthday._d);
        };
        if (values.editEventDate !== undefined) {
            editEventDate = await convertDate(values.editEventDate._d);
        };
        if (values.editExistingYear !== undefined) {
            editExistingYear = await convertYear(values.editExistingYear._d)
        };
        var obj = {
            id: sessionStorage.customer_id,
            name: values.editName === undefined ? '' : values.editName,
            gender: values.editSex,
            come_date: come_date,
            type: values.editType,
            event_info: values.editEventInfo === undefined ? '' : values.editEventInfo,
            event_place: values.editEventPlace === undefined ? '' : values.editIntroducer,
            event_date: editEventDate === undefined ? '' : editEventDate,
            Introducer: values.editIntroducer === undefined ? '' : values.editIntroducer, //介紹人
            license_plate: values.editLicense === undefined ? '' : values.editLicense,//現有客戶
            birthday: editBirthday === undefined ? '' : editBirthday,
            phone1: values.editPhoneType === undefined ? '' : values.PhoneType + values.editPhoneType,
            phone2: values.editPhoneType1 === undefined ? '' : values.PhoneType1 + values.editPhoneType1,
            phone3: values.editPhoneType2 === undefined ? '' : values.PhoneType2 + values.editPhoneType2,
            phone4: values.editPhoneType3 === undefined ? '' : values.PhoneType3 + values.editPhoneType3,
            occupation: values.editOccupation === undefined ? '' : values.editOccupation,//職業
            city: this.state.editCity,
            area: this.state.editArea,
            address: this.state.address,
            city2: this.state.editCity2, //戶籍地址
            area2: this.state.editArea2, //戶籍地址
            address2: this.state.address2, //戶籍地址
            email: values.editEmail === undefined ? '' : values.editEmail,
            line: values.editLine === undefined ? '' : values.editLine,
            existing_brand: values.editExistingBrand === undefined ? '' : values.editExistingBrand,  //現有車-品牌
            existing_model: values.editExistingModel === undefined ? '' : values.editExistingModel,
            existing_year: editExistingYear === undefined ? '' : editExistingYear,
            buy_car_reason: values.editBuyCarReason === undefined ? '' : values.editBuyCarReason,
            remark: values.editRemarks === undefined ? '' : values.editRemarks,
        };
        console.log(obj)
        // let TabData = await ajaxApi(obj, "potential/updatetab1");

        // if (TabData != null) {
        //     success('新增成功!');
        // };

    };

    //聯絡地址
    handleProvinceChange = async city => {
        console.log(city);
        let getArea = await getCityAreaMenu(city);

        this.setState({
            getArea: getArea.data,
            editCity: city,
        });
    };

    //戶籍地址
    handleRegisteredChange = async city => {

        let getArea = await getCityAreaMenu(city);

        this.setState({
            getArea: getArea.data,
            editCity2: city
        });
    };

    onRegisteredChange = value => {
        this.setState({
            editArea2: value
        });
    };

    changeSelect = (val) => {
        console.log(val)
        this.setState({
            TypeVal: val
        })
    };

    onSecondCityChange = value => {
        this.setState({
            secondCity: value,
            editArea: value,
        });
    };

    handelInput = (e) => {
        this.setState({
            address2: e.target.value
        });
    };
    RegisteredInput = (e) => {
        this.setState({
            address2: e.target.value
        });
    };


    render() {
        console.log('this.state.contacts', this.state.contacts)
        let { getFieldDecorator, setFieldsValue } = this.props.form;
        let { contacts, editExistingBrand, getArea, tabData, getCity, editOccupation, selectedEventMenu, editType, TypeVal, editEventInfo, isopen } = this.state;
        console.log(tabData)
        const formItem = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 12 }
            }
        };
        const config = {
            rules: [
                { type: "object", required: true, message: "Please select time!" }
            ]
        };
        const { Option } = Select;
        return (
            <>
                <Form {...formItem} className="tab-content" >
                    <Row>
                        <Col xs={24} lg={12} md={24} sm={24}>
                            <Row>
                                <Form.Item label="姓名">
                                    {getFieldDecorator("editName", {
                                        initialValue: tabData.name,
                                        rules: [
                                            {
                                                required: true,
                                                message: "請輸入姓名"
                                            }
                                        ]
                                    })(
                                        <Input style={{ width: 265 }} name="editName" />
                                    )}
                                </Form.Item>
                                <Form.Item label="性別">
                                    {getFieldDecorator("editSex", {
                                        initialValue: tabData.gender,
                                        rules: [
                                            {
                                                required: true,
                                                message: "請輸入姓名"
                                            }
                                        ]
                                    })(
                                        <Radio.Group>
                                            <Radio value="M">男</Radio>
                                            <Radio value="F">女</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                                <Form.Item label="來店日期" >
                                    {getFieldDecorator("editComeDate", {
                                        ...config,
                                        initialValue: moment(tabData.come_date, null)
                                    })(<DatePicker style={{ width: 265 }} />)}
                                </Form.Item>

                                <Form.Item label="客戶來源" hasFeedback>
                                    {getFieldDecorator("editType", {
                                        initialValue: tabData.type,
                                        rules: [
                                            { required: true, message: "請選擇客戶來源!" }
                                        ]
                                    })(
                                        <Select
                                            name="editType"
                                            style={{ width: 265 }}
                                            placeholder="請選擇客戶來源"
                                            optionFilterProp="children"
                                            onChange={this.changeSelect}>
                                            {editType.map((item, idex) => (
                                                <Option value={item.id} key={idex}>
                                                    {item.sps_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>


                                {TypeVal == '001' || tabData.event_info !== "" ?
                                    <Form.Item label="訊息來源" hasFeedback>
                                        {getFieldDecorator("editEventInfo", {
                                            initialValue: tabData.event_info,
                                            rules: [
                                                { required: true, message: "請選擇訊息來源!" }
                                            ]
                                        })(
                                            <Select
                                                name="editEventInfo"
                                                style={{ width: 265 }}
                                                placeholder="請選擇訊息來源"
                                                optionFilterProp="children">
                                                {editEventInfo.map((item, idex) => (
                                                    <Option value={item.id} key={idex}>
                                                        {item.sps_name}
                                                    </Option>
                                                ))}
                                            </Select>
                                        )}
                                    </Form.Item> : null}

                                {TypeVal == '004' ? <Form.Item label="介紹人">
                                    {getFieldDecorator("editIntroducer", {
                                    })(
                                        <Input style={{ width: 265 }} name="editIntroducer" />
                                    )}
                                </Form.Item> : null}

                                {TypeVal == '005' ? <Form.Item label="車牌(現有客戶)">
                                    {getFieldDecorator("editLicense", {
                                    })(
                                        <Input style={{ width: 265 }} name="editLicense" />
                                    )}
                                </Form.Item> : null}

                                {TypeVal == '003' ? <Form.Item label="外展地點" hasFeedback>
                                    {getFieldDecorator("editEventPlace")(
                                        <Select
                                            name="editEventPlace"
                                            style={{ width: 265 }}
                                            placeholder="請選擇活動"
                                            optionFilterProp="children">
                                            {selectedEventMenu.map((item) => (
                                                <Option value={item.name} key={item.name}>{item.name}</Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item> : null}

                                {TypeVal == '003' ? <Form.Item label="外展日期" hasFeedback>
                                    {getFieldDecorator("editEventDate")(
                                        <DatePicker
                                            style={{ width: 265 }}
                                            placeholder="請外展日期"
                                        />
                                    )}
                                </Form.Item> : null}

                                <Form.Item label="生日" hasFeedback>
                                    {getFieldDecorator("editBirthday")(
                                        <DatePicker
                                            style={{ width: 265 }}
                                            placeholder="請選擇生日"
                                        />
                                    )}
                                </Form.Item>

                                <Form.Item label="職業" hasFeedback>
                                    {getFieldDecorator("editOccupation")(
                                        <Select
                                            name="editOccupation"
                                            showSearch
                                            style={{ width: 265 }}
                                            placeholder="請選擇"
                                            optionFilterProp="children">
                                            {editOccupation.map((item, idex) => (
                                                <Option value={item.id} key={idex}>
                                                    {item.sps_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>

                                {/* <Form.Item
                                    className="editPhone d-flex"
                                    label="聯絡方式"
                                    validateStatus={this.state.validateStatus}
                                    style={{ position: relative }}>
                                    {getFieldDecorator("editPhoneType", {

                                    })(
                                        <Input
                                            addonBefore={getFieldDecorator('PhoneType', {
                                                initialValue: "M"
                                            })(prefixSelector)}
                                            style={{ width: 240 }}
                                        />
                                    )}
                                    <Button
                                        type="primary"
                                        shape="circle"
                                        icon="plus"
                                        className="btnMinus"
                                        onClick={this.plus}
                                    />
                                </Form.Item> */}


                                {[1,2,3,4,5].map((time,i) => {
                                  console.log(i)
                                    if (i === 0) {
                                      return <ContactForm key={time} id="editPhonetype" type="Phonetype" onBtnClick={this.addContact} buttonIcon="plus" />
                                    }
                                    return (
                                      <ContactForm
                                        key={time}
                                        id={`editPhonetype${i + 1}`}
                                        type={`Phonetype${i + 1}`}
                                        onBtnClick={() => this.removeContact(time)}
                                        buttonIcon="minus"
                                      />
                                    );
                                  })}

                                {this.state.contacts.map((time, i) => {
                                    

                                })}

                                <Form.Item label="聯絡地址">
                                    <Row className="d-flex" style={{ width: 265 }}>
                                        <Select
                                            placeholder="請選擇"
                                            onChange={this.handleProvinceChange}>
                                            {getCity.map((item, idex) => (
                                                <Option value={item.City} key={idex}>
                                                    {item.City}
                                                </Option>
                                            ))}
                                        </Select>
                                        <Select
                                            placeholder="請選擇"
                                            onChange={this.onSecondCityChange}>
                                            {getArea.map((item, idex) => (
                                                <Option value={item.Area} key={idex}>
                                                    {item.Area}
                                                </Option>
                                            ))}
                                        </Select>
                                    </Row>
                                    <Input style={{ width: 265 }} name="addEmail" onChange={this.handelInput} />
                                </Form.Item>

                                <Form.Item label="戶籍地址">
                                    <Row className="d-flex" style={{ width: 265 }}>
                                        <Select
                                            placeholder="請選擇"
                                            onChange={this.handleRegisteredChange}>
                                            {getCity.map((item, idex) => (
                                                <Option value={item.City} key={idex}>
                                                    {item.City}
                                                </Option>
                                            ))}
                                        </Select>
                                        <Select
                                            placeholder="請選擇"
                                            onChange={this.onRegisteredChange}>
                                            {getArea.map((item, idex) => (
                                                <Option value={item.Area} key={idex}>
                                                    {item.Area}
                                                </Option>
                                            ))}
                                        </Select>
                                    </Row>
                                    <Input style={{ width: 265 }} name="addEmail" onChange={this.RegisteredInput} />
                                </Form.Item>

                                <Form.Item
                                    label="E-mail"
                                    validateStatus={this.state.validateStatus}>
                                    {getFieldDecorator("editEmail")(
                                        <Input style={{ width: 265 }} name="editEmail" />
                                    )}
                                </Form.Item>
                                <Form.Item
                                    label="@Line帳號"
                                    validateStatus={this.state.validateStatus}>
                                    {getFieldDecorator("editLine")(
                                        <Input style={{ width: 265 }} name="editLine" />
                                    )}
                                </Form.Item>
                            </Row>
                        </Col>
                        <Col xs={24} lg={12} md={24} sm={24}>
                            <Row>
                                <Form.Item label="現有車-品牌" hasFeedback>
                                    {getFieldDecorator("editExistingBrand")(
                                        <Select
                                            name="editExistingBrand"
                                            showSearch
                                            style={{ width: 265 }}
                                            placeholder="請選擇"
                                            optionFilterProp="children">
                                            {editExistingBrand.map((item, idex) => (
                                                <Option value={item.id} key={idex}>
                                                    {item.sps_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label="現有車-車型">
                                    {getFieldDecorator("editExistingModel")(
                                        <Input style={{ width: 265 }} name="editExistingModel" />
                                    )}
                                </Form.Item>

                                <Form.Item label="現有車-年式">
                                    {getFieldDecorator("editExistingYear", {})(
                                        <DatePicker
                                            open={isopen}
                                            mode="year"
                                            placeholder="請選擇年份"
                                            format="YYYY"
                                            onOpenChange={status => {
                                                //弹出日历和关闭日历的回调
                                                console.log(status);
                                                if (status) {
                                                    this.setState({ isopen: true });
                                                } else {
                                                    this.setState({ isopen: false });
                                                }
                                            }}
                                            onPanelChange={v => {
                                                //日历面板切换的回调
                                                setFieldsValue({ editExistingYear: v });
                                                this.setState({
                                                    isopen: false
                                                });
                                            }}
                                        />
                                    )}
                                </Form.Item>
                                <Form.Item label="打算">
                                    {getFieldDecorator("editBuyCarReason", {
                                        initialValue: "增購"
                                    })(
                                        <Radio.Group>
                                            <Radio value="增購">增購</Radio>
                                            <Radio value="換購">換購</Radio>
                                            <Radio value="首購">首購</Radio>
                                        </Radio.Group>
                                    )}
                                </Form.Item>

                                <Form.Item label="備註">
                                    {getFieldDecorator("editRemarks", {})(
                                        <TextArea rows={4} />
                                    )}
                                </Form.Item>
                            </Row>
                        </Col>
                    </Row>

                    <Button type="primary" htmlType="submit" onClick={this.handleSubmit} block>
                        儲存
                    </Button>
                </Form>
            </>
        );
    };
};

const TabImformationFrom = Form.create()(TabImformation);
export default TabImformationFrom;