import React, { Component } from 'react';
import {
    Row,
    Col,
    Form,
    Tabs,
    Input,
    DatePicker,
    Select,
    Button,
    Radio,
    Checkbox,
    Modal,
    Collapse,
    Icon,
    Card,
} from "antd";

import moment from "moment";
import "moment/locale/zh-cn";

import { ajaxApi, getMenu, searchDetail, getCarMenu, getCityAreaMenu, getCarDetailMenu, getEventMenu, getCarDetailCodeMenu, googleCheckLink, googleCancel, writeTab, getList } from "../function/Api";
import { convertDate, success, setNextContactDate } from '../function/function';

moment.locale("zh-cn");
const dateFormat = "YYYY/MM/DD";
const { TextArea } = Input;
const { Panel } = Collapse;
const confirm = Modal.confirm;

const customPanelStyle = {
    background: '#f7f7f7',
    borderRadius: 4,
    marginBottom: 24,
    border: '1px solid #ccc',
    overflow: 'hidden',
};
class TabContact extends Component {

    state = {
        trackContactSite: [],
        trackContactType: [],
        nextContactType: [],
        getCarModel: [],
        ListDetail: [],
        tabData: [],
        TypeValue: '',
        nextDate: new Date(),
        ListDetail: [],
        text: '其他',
        state_m: 'N',
        state_t: 'N',
        state_p: 'N',
    }
    async componentDidMount() {
        let trackContactSite = await getMenu("tcs");
        let trackContactType = await getMenu("tct");
        let nextContactType = await getMenu('tct');
        let getCarModel = await getCarDetailCodeMenu();
        let tabData = await searchDetail();
        let ListDetail = await getList(1);
        this.setState({
            tabData: tabData.data[0],
            ListDetail: ListDetail.data,
            nextContactType: nextContactType.data,
            trackContactSite: trackContactSite.data,
            trackContactType: trackContactType.data,
            getCarModel: getCarModel.data,
        });
    };

    /**
     * 本次接觸事項
     */
    ChangeType = (TypeValue) => {
        this.setState({
            TypeValue: TypeValue
        });
    };

    /**
   * 購車熱度
   */
    onCheckbox = (ckValues) => {
        console.log(ckValues)
        if (ckValues.length === 3) {
            let nextDate = setNextContactDate(3);
            this.setState({
                text: 'H級',
                nextDate: nextDate
            });
        } else if (ckValues.length === 2 && !ckValues.includes("P")) {
            console.log(ckValues)
            let nextDate = setNextContactDate(7);
            this.setState({
                text: 'A級',
                nextDate: nextDate
            });
        } else if (ckValues.length === 2 && !ckValues.includes("T")) {
            let nextDate = setNextContactDate(30);
            this.setState({
                text: 'B級',
                nextDate: nextDate
            });
        } else if (ckValues.length === 1 && ckValues.includes("M")) {
            let nextDate = setNextContactDate(90);
            this.setState({
                text: 'C級',
                nextDate: nextDate,
                state_m: 'Y'
            });
        } else {
            if (ckValues.length === 1 && ckValues.includes("T")) {
                this.setState({
                    state_t: 'Y'
                });
            } else if (ckValues.length === 1 && ckValues.includes("P")) {
                this.setState({
                    state_t: 'Y'
                });
            }
            let nextDate = setNextContactDate(140);
            this.setState({
                text: '其他',
                nextDate: nextDate
            });
        };
    };

    GoogleCheck = async (ckValues) => {
        console.log(ckValues)
        if (ckValues.length === 1) {
            googleCheckLink();
        } else {
            this.showConfirm();
        };
    };

    showConfirm = async () => {
        await confirm({
            title: '是否完全移除與 Google 帳號的連結?',
            okText: '確定',
            cancelText: '取消',
            onOk: async () => {
                googleCancel();
            },
            onCancel() {
                console.log('取消');
            },
        });
    };


    trackSave = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log(values);
            if (!err) {
                console.log("Received values of form: ", values);
                this.trackSaveTab();

            };
        });
    };

    trackSaveTab = async () => {
        let values = this.props.form.getFieldsValue();
        console.log(values)
        let trackContactDate = await convertDate(values.trackContactDate._d);
        let trackTestBirthday, isGoogle;
        if (values.trackTestBirthday !== undefined) {
            trackTestBirthday = await convertDate(values.trackTestBirthday._d);
        };

        if (values.isGoogle !== undefined) {
            isGoogle = values.isGoogle.length > 0 ? "true" : "false";
        }

        let trackNextContactDate = await convertDate(values.trackNextContactDate._d);
        var obj = {
            id: sessionStorage.customer_id,
            state_m: this.state.state_m,
            state_t: this.state.state_t,
            state_p: this.state.state_p,
            state: this.state.text,
            contact_date: trackContactDate,
            contact_site: values.trackContactSite,
            contact_type: values.trackContactType,
            test_model: values.trackContactSite,
            test_id: '',
            test_birthday: trackTestBirthday === undefined ? '' : values.trackTestBirthday,
            test_address: values.trackTestAddress === undefined ? '' : values.trackTestAddress,
            test_phone: values.trackTestPhone === undefined ? '' : values.trackTestPhone,
            next_contact_date: trackNextContactDate,
            next_contact_point: values.trackNextContactPoint,
            remark: values.trackRemarks === undefined ? '' : values.trackRemarks,
            isGoogle: isGoogle === undefined ? false : isGoogle
        };
        console.log(obj);
        let SaveTab = await ajaxApi(obj, "potential/createtrack");

        if (SaveTab.msg === "新增成功！") {
            success('新增成功!');
            await this.searchDetail();
            let ListDetail = await getList(1);
            this.setState({
                ListDetail: ListDetail.data,
            });
        };
    };

    render() {
        const formItem = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 12 }
            }
        };
        const { Option } = Select;
        let { getFieldDecorator, setFieldsValue } = this.props.form;
        const { tabData, ListDetail, text, nextDate, trackContactSite, trackContactType, getCarModel, nextContactType, TypeValue } = this.state;
        const config = {
            rules: [
                { type: "object", required: true, message: "Please select time!" }
            ]
        };
        return (
            <>
                <Form {...formItem} className="tab-content">
                    <Row>
                        <Col xs={24} lg={12} md={24} sm={24}>
                            <Row>
                                <Form.Item>
                                    <Checkbox.Group
                                        style={{ width: "100%" }}
                                        onChange={this.onCheckbox}>
                                        <Row>
                                            <Col span={8}>
                                                <Checkbox value="M">預算</Checkbox>
                                            </Col>
                                            <Col span={8}>
                                                <Checkbox value="T">時間</Checkbox>
                                            </Col>
                                            <Col span={8}>
                                                <Checkbox value="P">偏好度</Checkbox>
                                            </Col>
                                        </Row>
                                    </Checkbox.Group>
                                </Form.Item>
                                <Form.Item label="本次接觸日期">
                                    {getFieldDecorator("trackContactDate", {
                                        ...config,
                                        initialValue: moment(new Date(), dateFormat)
                                    })(<DatePicker style={{ width: 300 }} />)}
                                </Form.Item>
                                <Form.Item label="本次接觸地點" hasFeedback>
                                    {getFieldDecorator("trackContactSite", {
                                        rules: [
                                            { required: true, message: "請選擇本次接觸地點!" }
                                        ]
                                    })(
                                        <Select
                                            name="trackContactSite"
                                            showSearch
                                            style={{ width: 300 }}
                                            placeholder="請選擇"
                                            optionFilterProp="children">
                                            {trackContactSite.map((item, idex) => (
                                                <Option value={item.id} key={idex}>
                                                    {item.sps_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label="本次接觸事項" hasFeedback>
                                    {getFieldDecorator("trackContactType", {
                                        rules: [
                                            {
                                                required: true,
                                                message: "請選擇本次接觸事項!"
                                            }
                                        ]
                                    })(
                                        <Select
                                            name="trackContactType"
                                            style={{ width: 300 }}
                                            placeholder="請選擇"
                                            optionFilterProp="children"
                                            onChange={this.ChangeType}
                                        >
                                            {trackContactType.map((item, idex) => (
                                                <Option value={item.id} key={idex}>
                                                    {item.sps_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>

                                {TypeValue === '002' ? <Form.Item label="試車車型" hasFeedback>
                                    {getFieldDecorator("trackTestModel", {
                                        rules: [
                                            { required: true, message: "請選擇試車車型!" }
                                        ]
                                    })(
                                        <Select
                                            name="trackTestModel"
                                            showSearch
                                            style={{ width: 300 }}
                                            placeholder="請選擇"
                                            optionFilterProp="children">
                                            {getCarModel.map((item, idex) => (

                                                <Option value={item.code} key={idex}>
                                                    {item.model}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item> : null}

                                {TypeValue === "002" ? <Form.Item label="試車人生日" hasFeedback>
                                    {getFieldDecorator("trackTestBirthday")(
                                        <DatePicker
                                            style={{ width: 300 }}
                                            placeholder="請選擇生日"
                                        />
                                    )}
                                </Form.Item> : null}

                                {TypeValue === "002" ? <Form.Item label="試車人地址" hasFeedback>
                                    {getFieldDecorator("trackTestAddress")(
                                        <Input
                                            style={{ width: 300 }}
                                            name="trackTestAddress"
                                        />
                                    )}
                                </Form.Item> : null}

                                {TypeValue === "002" ? <Form.Item label="試車人電話" hasFeedback>
                                    {getFieldDecorator("trackTestPhone", {
                                        rules: [
                                            { required: true, message: "請輸入試車人電話!" }
                                        ]
                                    })(
                                        <Input
                                            style={{ width: 300 }}
                                            name="trackTestPhone"
                                        />
                                    )}
                                </Form.Item> : null}

                            </Row>
                        </Col>
                        <Col xs={24} lg={12} md={24} sm={24}>
                            <Row>
                                <Form.Item>
                                    <Row>
                                        <Col span={12}>購車熱度</Col>
                                        {getFieldDecorator("state", {})(
                                            <Col span={12} id="state">
                                                {text}
                                            </Col>
                                        )}
                                    </Row>
                                </Form.Item>
                                <Form.Item label="下次接觸日期">
                                    {getFieldDecorator("trackNextContactDate", {
                                        ...config,
                                        initialValue: moment(nextDate, dateFormat)
                                    })(<DatePicker style={{ width: 300 }} />)}
                                </Form.Item>
                                <Form.Item label="下次接觸事項" hasFeedback>
                                    {getFieldDecorator("trackNextContactPoint", {
                                        rules: [
                                            { required: true, message: "請選擇下次接觸事項!" }
                                        ]
                                    })(
                                        <Select
                                            name="trackNextContactPoint"
                                            style={{ width: 300 }}
                                            placeholder="請選擇訊息來源"
                                            optionFilterProp="children">
                                            {nextContactType.map((item, index) => (
                                                <Option value={item.id} key={index}>
                                                    {item.sps_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label="備註">
                                    {getFieldDecorator("trackRemarks", {})(
                                        <TextArea rows={4} />
                                    )}
                                </Form.Item>
                            </Row>
                        </Col>
                        <Col
                            xs={24}
                            lg={24}
                            md={24}
                            sm={24}
                            className="text-center">
                            {getFieldDecorator("isGoogle", {})(
                                <Checkbox.Group
                                    style={{ width: "100%" }}
                                    onChange={this.GoogleCheck}
                                >
                                    <Row className="my-3">
                                        <Col span={24}>
                                            <Checkbox value="checked">
                                                新增至Google日曆
                                                         </Checkbox>
                                        </Col>
                                    </Row>
                                </Checkbox.Group>
                            )}
                        </Col>
                    </Row>
                    <Button type="primary" block onClick={this.trackSave}>
                        儲存
                    </Button>
                    <Col xs={24}
                        lg={24}
                        md={24}
                        sm={24}>
                        <Collapse className="mt-3"
                            bordered={false}
                            expandIcon={({ isActive }) => <Icon type={isActive ? "zoom-out" : "zoom-in"} />}>
                            {ListDetail.map((item, index) => (
                                <Panel header={item.contact_date} key={index} style={customPanelStyle}>
                                    <Card title="追蹤紀錄">
                                        <Row>
                                            <Col className="text-secondary"
                                                xs={7}
                                                lg={7}
                                                md={7}
                                                sm={7}>
                                                <p>購車熱度：</p>
                                                <p>本次接觸日期：</p>
                                                <p>本次接觸地點：</p>
                                                <p>本次接觸事項：</p>
                                            </Col>

                                            <Col className="text-dark"
                                                xs={5}
                                                lg={5}
                                                md={5}
                                                sm={5}>
                                                <p>{item.state}</p>
                                                <p>{item.contact_date}</p>
                                                <p>{item.contact_site}</p>
                                                <p>{item.contact_type}</p>
                                            </Col>

                                            <Col className="text-secondary"
                                                xs={7}
                                                lg={7}
                                                md={7}
                                                sm={7}>
                                                <p>下次接觸日期：</p>
                                                <p>下次接觸事項：</p>
                                                <p>備註：</p>
                                            </Col>

                                            <Col className="text-dark"
                                                xs={5}
                                                lg={5}
                                                md={5}
                                                sm={5}>
                                                <p>{item.next_contact_date}</p>
                                                <p>{item.next_contact_point}</p>
                                                <p>{item.remark}</p>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Panel>
                            ))}
                        </Collapse>
                    </Col>
                </Form>
            </>
        );
    }
}

const TabContactForm = Form.create()(TabContact);
export default TabContactForm;