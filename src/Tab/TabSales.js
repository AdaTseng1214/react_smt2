import React, { Component } from 'react';
import {
    Row,
    Col,
    Form,
    Input,
    Select,
    Modal,
    Icon,
    Card,
} from "antd";
import { searchDetail,ajaxApi, getMenu, changeState } from "../function/Api";
import { success } from '../function/function';

const { TextArea } = Input;
const confirm = Modal.confirm;
const { Option } = Select;
const customer_id = sessionStorage.customer_id;

class TabSales extends Component {

    state = {
        visible: false,
        failed: [],
        tabData: [],
    }


    /**
    * model 戰敗
    */
    GameOver = async () => {
        let failed = await getMenu("brd");

        this.setState({
            visible: true,
            failed: failed.data
        });
    };


    /**
     * 確認戰敗
     */
    handleOk =e => {
        e.preventDefault();
        this.props.form.validateFields(async(err, value) => {
            if (!err) {

                let ajaxResult =await changeState(customer_id, '戰敗', value.failedCompetition, value.failedCompetitionModel, value.failedCompetitionReason);

                success(ajaxResult);
                let tabData= await searchDetail();
                this.setState({
                    tabData:tabData.data[0],
                    visible: false,
                });
            };
        });

    };



    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    /** 
        * 確認無效
        * */
    invalidConfirm = async () => {
        await confirm({
            title: '無效',
            content: '即將更改客戶狀態，是否確認無效？',
            okText: '確認無效',
            cancelText: '取消',
            onOk: async () => {
                let ajaxResult = await changeState(customer_id, '無效', '', '', '');

                await success(ajaxResult);
                let tabData = await searchDetail();
                console.log(tabData)
                this.setState({
                    tabData:tabData.data[0]
                });
            },
            onCancel() {
                console.log('取消');
            },
        });
    };

    tabSales = () => {
        window.location = "/SMT/Front/PotentialClient/StockList.html";
    };


    render() {

        let { getFieldDecorator, setFieldsValue } = this.props.form;
        const { failed, visible } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 12 },
            },
        };

        return (
            <>
                <Form {...formItemLayout}>
                    <Row gutter={16}>
                        <Col xs={8} lg={8} md={24} sm={24}>
                            <Card bordered={false} className="SalesCard card-shadow" onClick={this.tabSales}>
                                <div className="d-flex align-items-center">
                                    <div style={{ fontSize: '30px' }} className="text-info p-2">
                                        <Icon type="check"></Icon>
                                    </div>
                                    <div>
                                        <h3 className="text-info  font-weight-bold m-0">簽訂、保留</h3>
                                        <p className="text-info m-0">簽訂、保留車子，建立訂單。</p>
                                    </div>
                                    <div className="text-secondary ml-auto">
                                        <Icon type="right" />
                                    </div>
                                </div>
                            </Card>
                        </Col>
                        <Col xs={8} lg={8} md={24} sm={24} onClick={this.invalidConfirm}>
                            <Card bordered={false} className="SalesCard card-shadow">
                                <div className="d-flex align-items-center">
                                    <div style={{ fontSize: '30px' }} className="text-blue p-2">
                                        <Icon type="eye-invisible" theme="filled" />
                                    </div>
                                    <div>
                                        <h3 className="text-blue font-weight-bold m-0">無效</h3>
                                        <p className="text-blue m-0">無法聯繫成功，或其他無效原因。</p>
                                    </div>
                                    <div className="text-secondary ml-auto">
                                        <Icon type="right" />
                                    </div>
                                </div>
                            </Card>
                        </Col>
                        <Col xs={8} lg={8} md={24} sm={24}>
                            <Card bordered={false} className="SalesCard card-shadow" onClick={this.GameOver}>
                                <div className="d-flex align-items-center">
                                    <div style={{ fontSize: '30px' }} className="text-danger p-2">
                                        <Icon type="close" />
                                    </div>
                                    <div>
                                        <h3 className="text-danger font-weight-bold m-0">戰敗</h3>
                                        <p className="text-danger m-0">客戶無購買意願。</p>
                                    </div>
                                    <div className="text-secondary ml-auto">
                                        <Icon type="right" />
                                    </div>
                                </div>
                            </Card>

                            <Modal wrapClassName="GameOverModel"
                                title="戰敗"
                                okText={'確認'}
                                cancelText={'取消'}
                                visible={this.state.visible}
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}>
                                <h3>請輸入戰敗原因--比較競品：</h3>
                                <Row>
                                    <Col xs={24} lg={12}>
                                        <Row>
                                            <Form.Item label="品牌" className="d-flex">
                                                {getFieldDecorator("failedCompetition")(
                                                    <Select
                                                        name="failedCompetition"
                                                        style={{ width: 370 }}
                                                        placeholder="請選擇"
                                                        optionFilterProp="children">
                                                        {failed.map((item, index) => (

                                                            <Option value={item.id} key={index}>
                                                                {item.sps_name}
                                                            </Option>
                                                        ))}

                                                    </Select>
                                                )}
                                            </Form.Item>
                                            <Form.Item label="車型" className="d-flex">
                                                {getFieldDecorator('failedCompetitionModel')(<Input style={{ width: 370 }} name="failedCompetitionModel" />)}
                                            </Form.Item>
                                            <Form.Item label="原因" className="TextAreaModel d-flex">
                                                {getFieldDecorator('failedCompetitionReason', {
                                                    rules: [{ required: true, message: '請輸入原因!' }],
                                                })(<TextArea autosize={{minRows: 5, maxRows:6}}/>
                                                )}
                                            </Form.Item>
                                        </Row>
                                    </Col>
                                </Row>
                            </Modal>
                        </Col>
                    </Row>
                </Form>
            </>
        );
    }
}

const TabSalesForm = Form.create()(TabSales);
export default TabSalesForm;