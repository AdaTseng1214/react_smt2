import React, { Component } from 'react';
import {
    Row,
    Col,
    Form,
    Tabs,
    Input,
    DatePicker,
    Select,
    Button,
    Radio,
    Checkbox,
    Modal,
    Collapse,
    Icon,
    Card,
} from "antd";
import { ajaxApi, searchDetail, getMenu, getCarMenu, getCityAreaMenu, getCarDetailMenu, getEventMenu, getCarDetailCodeMenu, googleCheckLink, googleCancel, writeTab, getList } from "../function/Api";
class TabPurchase extends Component {
    state = {
        editModel1: [],
        editCompetition: [],
        editColor: [],
        editPaymentMethod: [],
        getEditPower: [],
        getCarDetail: [],
        tabData: [],
    };

    onChangePower = async (value) => {
        this.props.form.resetFields('editVariant');
        let getEditPower = await getCarDetailMenu(this.state.carValue, value, 'variants');
        this.setState({
            getEditPower: getEditPower.data
        });
    };

    onChangecar = async (carValue) => {
        console.log(carValue)
        this.props.form.resetFields('editPower');
        let getCarDetail = await getCarDetailMenu(carValue, '', 'powertrain');
        this.setState({
            getCarDetail: getCarDetail.data,
            carValue: carValue,
        });

    };


    async componentDidMount() {
        let editModel1 = await getCarMenu();
        let editCompetition = await getMenu("brd");
        let editColor = await getMenu("col");
        let editPaymentMethod = await getMenu("pm");

        let tabData = await searchDetail();
        this.setState({
            editModel1: editModel1.data,
            editCompetition: editCompetition.data,
            editColor: editColor.data,
            editPaymentMethod: editPaymentMethod.data,
        });
    }
    render() {
        let { tabData, getCarDetail, getEditPower, editModel1, editCompetition, editColor, editPaymentMethod } = this.state;
        let { getFieldDecorator, setFieldsValue } = this.props.form;
        const { Option } = Select;

        const formItem = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 12 }
            }
        };
        const config = {
            rules: [
                { type: "object", required: true, message: "Please select time!" }
            ]
        };
        return (
            <>
                <Form {...formItem} className="tab-content" >
                    <Row>
                        <Col xs={24} lg={12} md={24} sm={24}>
                            <Row>
                                <Form.Item label="考慮車型" hasFeedback>
                                    {getFieldDecorator("editModel1", {
                                        initialValue: tabData.car_model1 ? tabData.car_model1 : undefined
                                    })(
                                        <Select
                                            name="editModel1"
                                            style={{ width: 265 }}
                                            placeholder="請選擇考慮車型"
                                            onChange={this.onChangecar}
                                            optionFilterProp="children">
                                            {editModel1.map((item, idex) => (
                                                <Option value={item.cbip_name} key={idex}>
                                                    {item.cbip_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>

                                <Form.Item label="考慮動力" hasFeedback>
                                    {getFieldDecorator("editPower", {
                                        initialValue: tabData.car_powertrain ? tabData.car_powertrain : undefined
                                    })(
                                        <Select
                                            name="editPower"
                                            style={{ width: 265 }}
                                            placeholder="請選擇考慮動力"
                                            onChange={this.onChangePower}
                                            optionFilterProp="children">
                                            {getCarDetail.map((item, index) => (
                                                // console.log(item)
                                                <Option value={item.sc} key={index}>
                                                    {item.sc}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label="考慮車款" hasFeedback>
                                    {getFieldDecorator("editVariant", {
                                        initialValue: tabData.car_variants ? tabData.car_variants : undefined
                                    })(
                                        <Select
                                            name="editVariant"
                                            style={{ width: 265 }}
                                            placeholder="請選擇考慮車款"
                                            optionFilterProp="children">
                                            {getEditPower.map((item, index) => (
                                                <Option value={item.sc} key={index}>
                                                    {item.sc}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>

                                <Form.Item label="比較競品-品牌" hasFeedback>
                                    {getFieldDecorator("editCompetition", {
                                        initialValue: tabData.car_competition ? tabData.car_competition : undefined
                                    })(
                                        <Select setFieldsValue={undefined}
                                            name="editCompetition"
                                            showSearch
                                            style={{ width: 265 }}
                                            placeholder="請選擇訊息來源"
                                            optionFilterProp="children">
                                            {editCompetition.map((item, index) => (
                                                <Option value={item.id} key={index}>
                                                    {item.sps_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>

                                <Form.Item
                                    label="比較競品-車型"
                                    validateStatus={this.props.validateStatus}>
                                    {getFieldDecorator("editCompetitionModel", {
                                        initialValue: tabData.car_competition_model ? tabData.car_competition_model : undefined
                                    })(
                                        <Input
                                            style={{ width: 265 }}
                                            name="editCompetitionModel"
                                        />
                                    )}
                                </Form.Item>
                            </Row>
                        </Col>
                        <Col xs={24} lg={12} md={24} sm={24}>
                            <Row>
                                <Form.Item label="喜好車色" hasFeedback>
                                    {getFieldDecorator("editColor", {
                                        initialValue: tabData.car_color ? tabData.car_color : undefined
                                    })(
                                        <Select
                                            name="editColor"
                                            showSearch
                                            style={{ width: 265 }}
                                            placeholder="請選擇車色"
                                            optionFilterProp="children"
                                        >
                                            {editColor.map((item, idex) => (
                                                <Option value={item.id} key={idex}>
                                                    {item.sps_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label="付款方式" hasFeedback>
                                    {getFieldDecorator("editPaymentMethod", {
                                        initialValue: tabData.payment_method ? tabData.payment_method : undefined
                                    })(
                                        <Select
                                            name="editPaymentMethod"
                                            showSearch
                                            style={{ width: 265 }}
                                            placeholder="請選擇"
                                            optionFilterProp="children"
                                        >
                                            {editPaymentMethod.map((item, idex) => (
                                                <Option value={item.id} key={idex}>
                                                    {item.sps_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>
                            </Row>
                        </Col>
                    </Row>
                    <Button type="primary" block onClick={this.purchasePlan}>
                        儲存
                    </Button>
                </Form>
            </>
        );
    }
}


const TabPurchaseForm = Form.create()(TabPurchase);
export default TabPurchaseForm;