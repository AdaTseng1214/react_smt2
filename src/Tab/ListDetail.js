import React, { Component } from 'react';
import {
    Row,
    Col,
    Form,
    Collapse,
    Icon,
    Card,

} from "antd";
const { Panel } = Collapse;

const customPanelStyle = {
    background: '#f7f7f7',
    borderRadius: 4,
    marginBottom: 24,
    border: '1px solid #ccc',
    overflow: 'hidden',
};

class ListDetail extends Component {
    render() {
        console.log(this.props.data)
        return (
            <>
                <Collapse className="mt-3"
                    bordered={false}
                    expandIcon={({ isActive }) => <Icon type={isActive ? "zoom-out" : "zoom-in"} />}>
                    {this.props.data.map((item, index) => (
                        <Panel header={item.contact_date} key={index} style={customPanelStyle}>
                            <Card title="追蹤紀錄">
                                <Row>
                                    <Col className="text-secondary"
                                        xs={7}
                                        lg={7}
                                        md={7}
                                        sm={7}>
                                        <p>購車熱度：</p>
                                        <p>本次接觸日期：</p>
                                        <p>本次接觸地點：</p>
                                        <p>本次接觸事項：</p>
                                    </Col>

                                    <Col className="text-dark"
                                        xs={5}
                                        lg={5}
                                        md={5}
                                        sm={5}>
                                        <p>{item.state}</p>
                                        <p>{item.contact_date}</p>
                                        <p>{item.contact_site}</p>
                                        <p>{item.contact_type}</p>
                                    </Col>

                                    <Col className="text-secondary"
                                        xs={7}
                                        lg={7}
                                        md={7}
                                        sm={7}>
                                        <p>下次接觸日期：</p>
                                        <p>下次接觸事項：</p>
                                        <p>備註：</p>
                                    </Col>

                                    <Col className="text-dark"
                                        xs={5}
                                        lg={5}
                                        md={5}
                                        sm={5}>
                                        <p>{item.next_contact_date}</p>
                                        <p>{item.next_contact_point}</p>
                                        <p>{item.remark}</p>
                                    </Col>
                                </Row>
                            </Card>
                        </Panel>
                    ))}
                </Collapse>
            </>
        );
    }
}

const ListDetailForm = Form.create()(ListDetail);
export default ListDetailForm;