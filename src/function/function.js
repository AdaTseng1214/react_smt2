// const IP = location.protocol + "//" + window.location.host;
import { Modal } from "antd";


/**
    * 轉換日期 2019-09-09 ----> 20190909
    * @param {String} formVals
    */
const convertDate = async (formVals) => {

        let date = formVals
        let yy = date.getFullYear();
        var mm = (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1);
        let dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
        let apiName = (yy + mm + dd)
        return apiName;
};

/**
 * 轉換年 2019-09-09 ----> 2019
 * @param {String} formVals 
 */
const convertYear = async (formVals) => {
        let date = formVals
        let yy = date.getFullYear();
        var mm = (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1);
        let dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
        let apiName = (yy + mm + dd);
        let num = apiName.substr(0, 4);
        return num;
};

const pageTotal = async (pagecount) => {
        let count = pagecount * 10;
        // let v = parseInt(count);
        return count;
}
        ;
/**
 *  成功時跳出model
 */
const success = async (title, content = "") => {
        Modal.success({
                title: title,
                content: content,
        });
};

/**
 * 變更接觸日期
 * @param {String} day 
 */
const setNextContactDate = (day) => {
        var dd = new Date();
        dd.setDate(dd.getDate() + day);
        var nd = dd.getFullYear() + '-' + ((dd.getMonth() + 1) < 10 ? '0' : '') + (dd.getMonth() + 1) + '-' + (dd.getDate() < 10 ? '0' : '') + dd.getDate();
        return nd;
};

/** error model
 *  @param {String} title 
 * @param {String} content 
 */
const errorModel = (title, content = "") => {
        Modal.error({
                title: title,
                content: content,
                okText: '知道了'
        });
};

/**
 * 轉base64
 * @param {Num} file 
 */

const getBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
};


const renderImg = (data) => {
        console.log(data);
    };


export {
        convertDate,
        pageTotal,
        convertYear,
        success,
        setNextContactDate,
        errorModel,
        getBase64,
        renderImg
};