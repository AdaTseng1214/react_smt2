const ip = "http://smt.genesys-tech.com:777/smt/smt/";
// const IP = location.protocol + "//" + window.location.host;

const ajaxApi = async (obj, functionName) => {
    let api_call = await fetch(ip + functionName, {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + sessionStorage.getItem('token')
        },
        body: JSON.stringify(obj),
        async: false,

    }).then(api_call => {
        return api_call.json();
    }).then((result) => {
        console.log("========= API =========");
        console.log(result);
        return result;
    }).catch((error) => {
        switch (error) {
            case "ipError":
                alert("偵測到IP變更，請重新登入。");
                break;
            case "authorizationError":
                alert("下載資料失敗，請重新登入。");
                break;
            default:
                alert("發生未預期的錯誤...請聯絡系統管理員。");
                break;
        };
        console.log(error);
        return false;
    });
    return api_call;
};

const ajaxLogin = async (obj) => {
    console.log(obj);
    console.log(ip + 'token/gettoken');
    fetch(ip + 'token/gettoken', {
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            method: 'POST',
            body: JSON.stringify(obj),
            async: false,
        }).then(res => res.json())
        .then(data => {
            console.log(data);
            sessionStorage.setItem('token', data.token);
            sessionStorage.setItem('userid', data.userid);
            sessionStorage.setItem('username', data.level[0].name);
            sessionStorage.setItem('level', data.level[0].level);
            sessionStorage.setItem('homepage', data.level[0].home_page);
            sessionStorage.removeItem('data');
            if (sessionStorage.getItem('pathname') != null) {

            } else {
                window.location = "/SMT/Front/Index/Index.html";
            };



        })
        .catch(e => alert('帳號或是密碼輸入錯誤！', e));
};


const setCookie = (cname, cvalue, exdays) => {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//select end
const getMenu = async (sppid) => {
    if (localStorage.getItem(sppid) != null) {} else {
        var obj = {
            sppid: sppid
        };
        console.log(obj)
        let selectedOption = await ajaxApi(obj, "code/searchdetailmenu");
        console.log(selectedOption)
        return selectedOption;
    };
};


//car master menu
const getCarMenu = async () => {
    if (localStorage.getItem('CarMenu') != null) {

    } else {
        var obj = {};
        let selectedOption = await ajaxApi(obj, "carinformation/searchmastermenu");
        return selectedOption;
    };
};

//event menu
const getEventMenu = async () => {
    if (localStorage.getItem('EventMenu') != null) {

    } else {
        var obj = {};
        let selectedEventMenu = await ajaxApi(obj, "event/searchmenu");
        return selectedEventMenu;
    };
};

//考慮車型
const getCarDetailMenu = async (cbipid, powertrain, column) => {
    var obj = {
        cbipid: cbipid,
        powertrain: powertrain,
        column: column
    };

    let selectedOption = await ajaxApi(obj, "carinformation/searchdetailmenu");
    return selectedOption;
};


//test_model
const getCarDetailCodeMenu = async () => {
    var obj = {};

    let menu = ajaxApi(obj, "carinformation/searchdetailnamemenu");
    return menu;
};

const getTrackDate = async (p, tableID) => {
    let obj = {
        p: p,
        type: tableID.replace(/#/g, "")
    }
    let ajaxResult = await ajaxApi(obj, "frontindex/searchtrackdate");
    return ajaxResult;
};

//今日活動
const getTrackCount = async () => {
    var obj = {};
    let ajaxResult = await ajaxApi(obj, "frontindex/searchtrackdatecount");
    return ajaxResult;
};

//H級
const getStateDetail = async (p, state) => {
    var obj = {
        p: p,
        state: state
    };

    let ajaxResult = ajaxApi(obj, "frontindex/searchstate");
    return ajaxResult;
};

const getOrderDetail = async (p, order_state) => {
    var obj = {
        p: p,
        order_state: order_state
    };
    console.log(obj)
    let ajaxResult = ajaxApi(obj, "frontindex/searchorderstatedetail");
    return ajaxResult;
};



//get city & area menu
const getCityAreaMenu = async (city = "") => {
    var obj = {
        city: city
    };

    let ajaxResult = ajaxApi(obj, "potentialpublic/searchcityareamenu");
    return ajaxResult;
};

/**
 * 綁定 google
 */
const googleCheckLink = async () => {
    let google = await ajaxApi({}, "googletoken/getgoogleoauthcode");
    console.log(google)
    if (google != null) {
        if (google.RedirectUri != null && google.credential == null) {
            window.open(google.RedirectUri);
        }
    };
};

/**
 * 移除 google
 */
const googleCancel = async () => {
    let googleValidation = await ajaxApi({}, "googletoken/getgoogleoauthcode");
    console.log(googleValidation)
    let arrRes;
    if (googleValidation.credential != null) {
        arrRes = await googleValidation.credential.split(":");
        await ajaxApi({
            userid: arrRes[1],
            token: arrRes[0]
        }, "googletoken/revokegoogleoauth");
    };
};

const writeTab = async () => {
    var obj = {
        id: sessionStorage.customer_id
    };
    let tabData = await ajaxApi(obj, "potential/searchupdatetab1");
    return tabData;
};

const getList = async (p) => {
    var obj = {
        p: p,
        id: sessionStorage.customer_id
    };

    let Data = await ajaxApi(obj, "potential/searchtrack");
    return Data;
};


//search stock column
const getStockMenu = async (year) => {

    var obj = {
        column: year
    };

    let Menu = await ajaxApi(obj, "stock/searchcolumnmenu");
    return Menu;
};

/**
 * Tab Detail
 */
const searchDetail = async () => {
    let tabData = await writeTab();
    return tabData;
};

/**
 * 簽訂
 */
const SigningContract = async () => {

    let obj = {
        // FO: $('#FO').html().replace(/\ /g, ''),
        // model: $('#model').attr('data-model'),
        // year: $('#year').html(),
        // color: $('#color').html(),
        // interior: $('#interior').html(),
        // option_code: $('#option_code').html(),
        // date_ETA: $('#date_ETA').html(),
        // id: localStorage.getItem('customer_id'),
        // order_state: order_state,
        // order_id: orderId
    };
    console.log(obj)
    let Contract = await ajaxApi(obj, "orders/frontdeal");
    return Contract;
};

const  changeState =  (id,state,failed_competition,failed_competition_model,failed_competition_reason) => {
   let obj = {
        id: id,
        state: state,
        failed_competition: failed_competition,
        failed_competition_model: failed_competition_model,
        failed_competition_reason: failed_competition_reason
    };
    console.log(obj)
    let ajaxResult = ajaxApi(obj, "potential/changestate");
    return ajaxResult;
};




export {
    ajaxApi,
    ip,
    ajaxLogin,
    getMenu,
    getTrackDate,
    getTrackCount,
    getStateDetail,
    getOrderDetail,
    getCarMenu,
    setCookie,
    getCityAreaMenu,
    getCarDetailMenu,
    getEventMenu,
    getCarDetailCodeMenu,
    googleCheckLink,
    googleCancel,
    writeTab,
    getList,
    getStockMenu,
    searchDetail,
    SigningContract,
    changeState
    
};