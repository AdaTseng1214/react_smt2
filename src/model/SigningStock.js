import React, { Component } from 'react'
import { Modal, Button, Alert } from 'antd';


class SigningStock extends Component {
    state = {
        // ModalText: '簽訂期貨庫存',
        visible: false,
        confirmLoading: false,
        noResultRemark: '',
        noResultInterior: ''
    };

    showModal = (e) => {
        e.preventDefault();
        this.setState({
            visible: true,
            noResultRemark: document.getElementById('noResultRemark').value,
            noResultInterior: document.getElementById('noResultInterior').value,
        });

    };

    handleOk = () => {
        this.setState({
            // ModalText: 'The modal will be closed after two seconds',
            confirmLoading: true,
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false,
            });
        }, 2000);
    };

    handleCancel = () => {
        console.log('Clicked cancel button');
        this.setState({
            visible: false,
        });
    };

    render() {

        const { visible, confirmLoading, noResultRemark, noResultInterior } = this.state;
        console.log(this.props)
        return (
            <div>
                <Button type="primary" onClick={this.showModal}>
                    簽訂期貨庫存
            </Button>
                <Modal
                    title="簽訂"
                    visible={visible}
                    onOk={this.handleOk}
                    confirmLoading={confirmLoading}
                    okText={'簽訂'}
                    cancelText={'取消'}
                    onCancel={this.handleCancel}>
                    <p>車型：{this.props.Select}</p>
                    <p>年式：{this.props.Year}</p>
                    <p>車色：{this.props.Color}</p>
                    <p>內裝：{noResultRemark}</p>
                    <p>特殊配備：{noResultInterior}</p>
                    <Alert message="即將成立訂單，是否確認簽訂？" type="info" showIcon />
                </Modal>
            </div>
        )
    };
};
export default SigningStock;