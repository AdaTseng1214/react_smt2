import React from 'react';
import { Form, Input, Radio, Row, Col, Alert, DatePicker, Select } from 'antd';
import { ajaxApi, getMenu, getEventMenu } from '../function/Api';
import moment from 'moment';
import 'moment/locale/zh-cn';
moment.locale('zh-cn');

const dateFormat = 'YYYY/MM/DD';

class ModelPage extends React.Component {

    async componentDidMount() {
        let selectedOptionA = await getMenu('typ');
        let selectedOptionB = await getMenu('col')
        let selectedOption = await getMenu('ifo')

        this.setState({
            selectedOptionA: selectedOptionA.data,
            selectedOptionB: selectedOptionB.data,
            selectedOption: selectedOption.data
        });
        let selectedEvent = await getEventMenu();
        this.setState({
            selectedEventMenu:selectedEvent.data
        });
    };

    state = {
        value: 1,
        selectedOption: [],
        selectedOptionA: [],
        selectedOptionB: [],
        selectedEventMenu: [],
    };




    //select start
    onChangeselect = (val) => {
        this.setState({
            value: val
        });
    };
    onChangedate = (date, dateString) => {
        console.log(dateString.length)
    };


    render() {

        const { value, selectedOptionA, selectedOptionB, selectedOption, selectedEventMenu } = this.state;
        let { getFieldDecorator } = this.props.form;
        const { Option } = Select;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 12 },
            },
        };
        const config = {
            rules: [{ type: 'object', required: true, message: 'Please select time!' }],

        };


        return (
            <Form {...formItemLayout}>
                <Row>
                    <Col xs={24} lg={12} md={12} sm={24}>
                        <Row>
                            <Form.Item label="姓名" >
                                {getFieldDecorator('addName', {
                                    rules: [
                                        {
                                            required: true,
                                            message: '請輸入姓名',
                                        },
                                    ],
                                })(<Input style={{ width: 300 }} name="addName" />)}
                            </Form.Item>
                            <Form.Item label="性別" >
                                {getFieldDecorator('modifier', {
                                    initialValue: 'M',
                                    rules: [
                                        {
                                            required: true,
                                            message: '請輸入姓名',
                                        },
                                    ],
                                })(
                                    <Radio.Group>
                                        <Radio value="M">男</Radio>
                                        <Radio value="F">女</Radio>
                                    </Radio.Group>,
                                )}
                            </Form.Item>

                            <Form.Item label="來店日期" >
                                {getFieldDecorator('come_date', { ...config, initialValue: moment(new Date(), dateFormat) })(<DatePicker name="come_date" id="come_date" style={{ width: 300 }} onChange={this.onChangedate} />)}
                            </Form.Item>
                            <Form.Item label="客戶來源" hasFeedback >
                                {getFieldDecorator('addType', {
                                    rules: [{ required: true, message: '請選擇客戶來源!' }],
                                })(
                                    <Select
                                        name="addType"
                                        showSearch
                                        style={{ width: 300 }}
                                        placeholder="請選擇客戶來源"
                                        optionFilterProp="children"
                                        onChange={this.onChangeselect}>
                                        {selectedOptionA.map((item) => (
                                            <Option value={item.id} key={item.id}>{item.sps_name}</Option>
                                        ))}
                                    </Select>
                                )}
                            </Form.Item>

                            {value === "001" ? <Form.Item label="訊息來源" hasFeedback >
                                {getFieldDecorator('addIfo', {
                                    rules: [{ required: true, message: '請選擇客戶來源!' }],
                                })(
                                    <Select
                                        name="addIfo"
                                        showSearch
                                        style={{ width: 300 }}
                                        placeholder="請選擇訊息來源"
                                        optionFilterProp="children">
                                        {selectedOption.map((item) => (
                                            <Option value={item.id} key={item.id}>{item.sps_name}</Option>
                                        ))}
                                    </Select>
                                )}
                            </Form.Item> : null}

                            {value === "003" ? <Form.Item label="外展地點" hasFeedback >
                                {getFieldDecorator('addEventPlace', {
                                    rules: [{ required: true, message: '請選擇客戶活動!' }],
                                })(<Select
                                    name="addEventPlace"
                                    style={{ width: 300 }}
                                    placeholder="請選擇活動"
                                    optionFilterProp="children">
                                    {selectedEventMenu.map((item) => (
                                        <Option value={item.name} key={item.name}>{item.name}</Option>
                                    ))}
                                </Select>
                                )}
                            </Form.Item> : null}

                            {value === "003" ? <Form.Item label="外展日期" >
                                {getFieldDecorator('event_date', config)(<DatePicker style={{ width: 300 }} />)}
                            </Form.Item> : null}

                            <Form.Item label="考慮車型" hasFeedback >
                                {getFieldDecorator('addModel1', {
                                    rules: [{ required: true, message: '請選擇客戶活動!' }],
                                })(<Select
                                    name="addModel1"
                                    showSearch
                                    style={{ width: 300 }}
                                    placeholder="請選擇考慮車型"
                                    optionFilterProp="children">
                                    {selectedOptionB.map((item) => (
                                        <Option value={item.id} key={item.id}>{item.sps_name}</Option>
                                    ))}
                                </Select>
                                )}
                            </Form.Item>
                        </Row>
                    </Col>
                    <Col xs={24} lg={12} md={12} sm={24}>
                        <Row>
                            <Form.Item >
                                <Alert style={{ width: 390 }} message="以下欄位（黃色*號）請擇一填寫" type="warning" />
                            </Form.Item>
                            <Form.Item label="電話" validateStatus={this.props.validateStatus} >
                                {getFieldDecorator('addPhone1')(<Input style={{ width: 300 }} name="addPhone1" />)}
                            </Form.Item>
                            <Form.Item label="E-mail" validateStatus={this.props.validateStatus} >
                                {getFieldDecorator('addEmail')(<Input style={{ width: 300 }} name="addEmail" />)}
                            </Form.Item>
                            <Form.Item label="@Line帳號" validateStatus={this.props.validateStatus} >
                                {getFieldDecorator('addLine')(<Input style={{ width: 300 }} name="addLine" />)}
                            </Form.Item>
                        </Row>
                    </Col>
                </Row>
            </Form >
        );
    };
};

const WrappedTimeRelatedForm = Form.create({ name: 'time_related_controls' })(ModelPage);
export default WrappedTimeRelatedForm;