import React from 'react';
import { Modal, Form, Input, Row, Col, DatePicker, Select, Checkbox } from 'antd';
import moment from 'moment';
import 'moment/locale/zh-cn';
import { ajaxApi, getMenu,googleCheckLink,googleCancel } from '../function/Api';



moment.locale('zh-cn');

const confirm = Modal.confirm;
const dateFormat = 'YYYY/MM/DD';
const { TextArea } = Input;
class Activity extends React.Component {

    async componentDidMount() {

        let trackContactSite = await getMenu('tcs');
        let trackContactType = await getMenu('tct');
        let trackNextContactPoint = await getMenu('tcp');
        let CarDetailCode = await this.getCarDetailCodeMenu();
        this.setState({
            trackContactSite: trackContactSite.data,
            trackContactType: trackContactType.data,
            trackNextContactPoint: trackNextContactPoint.data,
            CarDetailCode: CarDetailCode.data
        });
    };

    state = {
        value: 1,
        CarDetailCode: [],
        trackContactSite: [],
        trackNextContactPoint: [],
        trackContactType: [],
        text: '其他',
        date: new Date(),
    };


    //event menu
    getEventMenu = async () => {
        if (localStorage.getItem('EventMenu') != null) {

        } else {
            var obj = {};
            let selectedEventMenu = await ajaxApi(obj, "event/searchmenu");
            console.log(selectedEventMenu.data)
            this.setState({
                selectedEventMenu: selectedEventMenu.data
            });

        };
    };

    getCarDetailCodeMenu = async () => {
        if (sessionStorage.getItem('CarDetailNameMenu') != null) {

        } else {
            var obj = {};
            let CarDetailCode = await ajaxApi(obj, "carinformation/searchdetailnamemenu");
            return CarDetailCode;
        };
    };


    //select start
    onChangeselect = (val) => {
        this.setState({
            value: val
        });
    };


    setNextContactDate = (day) => {
        var dd = new Date();
        dd.setDate(dd.getDate() + day);
        var nd = dd.getFullYear() + '-' + ((dd.getMonth() + 1) < 10 ? '0' : '') + (dd.getMonth() + 1) + '-' + (dd.getDate() < 10 ? '0' : '') + dd.getDate();
        this.setState({
            date: nd,
        });
    };
    
    showConfirm = async () => {
        await confirm({
            title: '是否完全移除與 Google 帳號的連結?',
            okText: '確定',
            cancelText: '取消',
            onOk: async () => {
                googleCancel();
            },
            onCancel() {
                console.log('取消');
            },
        });
    };

    GoogleCheck = async (ckValues) => {
        if (ckValues.length === 1) {
            googleCheckLink();
        } else {
            this.showConfirm();
        };
    };

    /**
      * 購車熱度
      */
    onChange = (ckValues) => {
        if (ckValues.length === 3) {
            this.setState({
                text: 'H級',
            });
            this.setNextContactDate(3);

        } else if (ckValues.length === 2 && !ckValues.includes("P")) {
            this.setState({
                text: 'A級'
            });
            this.setNextContactDate(7);
        } else if (ckValues.length === 2 && !ckValues.includes("T")) {
            this.setState({
                text: 'B級'
            });
            this.setNextContactDate(30);
        } else if (ckValues.length === 1 && ckValues.includes("M")) {
            this.setState({
                text: 'C級'
            });
            this.setNextContactDate(90);
        } else {
            this.setState({
                text: '其他'
            });
            this.setNextContactDate(2);
        };

    };


    render() {
        const { text, value, CarDetailCode, trackNextContactPoint, trackContactSite, trackContactType } = this.state;
        let { getFieldDecorator } = this.props.form;
        const { Option } = Select;
        const Layout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const config = {
            rules: [{ type: 'object', required: true, message: 'Please select time!' }],

        };
        const rangeConfig = {
            rules: [{ type: 'array', required: true, message: 'Please select time!' }],
        };

        return (
            <Form {...Layout} >
                <Row>
                    <Col xs={24} lg={12}>
                        <Form.Item>
                            <Checkbox.Group style={{ width: '100%' }} onChange={this.onChange}>
                                <Row>
                                    <Col span={8}>
                                        <Checkbox value="M">預算</Checkbox>
                                    </Col>
                                    <Col span={8}>
                                        <Checkbox value="T">時間</Checkbox>
                                    </Col>
                                    <Col span={8}>
                                        <Checkbox value="P">偏好度</Checkbox>
                                    </Col>
                                </Row>
                            </Checkbox.Group>
                        </Form.Item>
                        <Row>

                            <Form.Item label="本次接觸日期">
                                {getFieldDecorator('trackContactDate', { ...config, initialValue: moment(new Date(), dateFormat) })(<DatePicker style={{ width: 265 }} />)}
                            </Form.Item>
                            <Form.Item label="本次接觸地點" hasFeedback>
                                {getFieldDecorator('trackContactSite', {
                                    rules: [{ required: true, message: '請選擇接觸地點!' }],
                                })(
                                    <Select
                                        name="trackContactSite"
                                        showSearch
                                        style={{ width: 265 }}
                                        placeholder="請選擇"
                                        optionFilterProp="children">
                                        {trackContactSite.map((item) => (
                                            <Option value={item.id} key={item.id}>{item.sps_name}</Option>
                                        ))}
                                    </Select>
                                )}
                            </Form.Item>

                            <Form.Item label="本次接觸事項" hasFeedback>
                                {getFieldDecorator('trackContactType', {
                                    rules: [{ required: true, message: '請選擇本次接觸事項!' }],
                                })(
                                    <Select
                                        name="trackContactType"
                                        showSearch
                                        style={{ width: 265 }}
                                        placeholder="請選擇"
                                        optionFilterProp="children"
                                        onChange={this.onChangeselect}>
                                        {trackContactType.map((item) => (
                                            <Option value={item.id} key={item.id}>{item.sps_name}</Option>
                                        ))}
                                    </Select>
                                )}
                            </Form.Item>

                            {value === "002" ? <Form.Item label="試車車型" hasFeedback>
                                {getFieldDecorator('trackTestModel', {
                                    rules: [{ required: true, message: '請選擇試車車型!' }],
                                })(
                                    <Select
                                        name="trackTestModel"
                                        showSearch
                                        style={{ width: 265 }}
                                        placeholder="請選擇試車車型"
                                        optionFilterProp="children">
                                        {CarDetailCode.map((item, idex) => (
                                            <Option value={item.code} key={idex}>{item.model}</Option>
                                        ))}
                                    </Select>
                                )}
                            </Form.Item> : null}

                            {value === "002" ? <Form.Item label="試車人生日" hasFeedback>
                                {getFieldDecorator('trackTestBirthday', {
                                })(<DatePicker style={{ width: 265 }} />)}
                            </Form.Item> : null}
                            {value === "002" ? <Form.Item label="試車人地址" hasFeedback>
                                {getFieldDecorator('trackTestAddress', {
                                })(<Input style={{ width: 265 }} name="addName" />)}
                            </Form.Item> : null}
                            {value === "002" ? <Form.Item label="試車人電話">
                                {getFieldDecorator('trackTestPhone', {
                                    rules: [
                                        {
                                            required: true,
                                            message: '請輸入試車人電話',
                                        },
                                    ],
                                })(<Input style={{ width: 265 }} name="trackTestPhone" />)}
                            </Form.Item> : null}
                        </Row>
                    </Col>
                    <Col xs={24} lg={12}>
                        <Row>
                            <Form.Item>
                                <Row>
                                    <Col span={12}>
                                        購車熱度
                                    </Col>
                                    {getFieldDecorator('state', {
                                    })(<Col span={12} id='state'>{text}</Col>)}
                                </Row>
                            </Form.Item>

                            <Form.Item label="下次接觸日期">
                                {getFieldDecorator('trackNextContactDate', { ...config, initialValue: moment(this.state.date, dateFormat) })(<DatePicker style={{ width: 265 }} />)}
                            </Form.Item>

                            <Form.Item label="下次接觸事項" hasFeedback>
                                {getFieldDecorator('trackNextContactPoint', {
                                    rules: [{ required: true, message: '請選擇客戶活動!' }],
                                })(<Select
                                    name="trackNextContactPoint"
                                    showSearch
                                    style={{ width: 265 }}
                                    placeholder="請選擇活動"
                                    optionFilterProp="children">
                                    {trackNextContactPoint.map((item) => (
                                        <Option value={item.id} key={item.id}>{item.sps_name}</Option>
                                    ))}
                                </Select>
                                )}
                            </Form.Item>
                            <Form.Item label="備註">
                                {getFieldDecorator('trackRemarks', {
                                })(<TextArea rows={4} />)}
                            </Form.Item>
                        </Row>
                    </Col>
                    <Col xs={24} lg={24} className="text-center">
                        {getFieldDecorator('isGoogle', {
                        })(<Checkbox.Group style={{ width: '100%' }} onChange={this.GoogleCheck}>
                            <Row>
                                <Col span={24}>
                                    <Checkbox value="checked">新增至Google日曆</Checkbox>
                                </Col>
                            </Row>
                        </Checkbox.Group>)}
                    </Col>
                </Row>
            </Form>
        );
    };
};

const PotentialClient = Form.create()(Activity);
export default PotentialClient;