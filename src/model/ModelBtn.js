import React from 'react';
import moment from 'moment';
import 'moment/locale/zh-cn';
import { Button, Modal, Icon } from 'antd';
import WrappedTimeRelatedForm from './ModelPage';
import PotentialClient from './Activity';
import { ajaxApi } from '../function/Api';
import { convertDate } from '../function/function';
moment.locale('zh-cn');


let come_date, event_date;

const confirm = Modal.confirm;
class ModelBtn extends React.Component {

    state = {
        loading: false,
        visible: '',
        validateStatus: '',
    };


    showModal = (e) => {
        e.preventDefault();
        this.setState({
            visible: 1,
            validateStatus: "",
        });
    };

    showConfirm = () => {
        confirm({
            title: '重複客戶',
            content: '此客戶已建檔，是否重複建立 ?',
            okText: '確定',
            cancelText: '取消',
            onOk: () => {
                this.setState({
                    visible: 2,
                });
            },
            onCancel() {
                sessionStorage.removeItem('name');
                sessionStorage.removeItem('gender');
                sessionStorage.removeItem('come_date')
                sessionStorage.removeItem('type');
                sessionStorage.removeItem('event_info');
                sessionStorage.removeItem('event_place');
                sessionStorage.removeItem('event_date');
                sessionStorage.removeItem('model1');
                sessionStorage.removeItem('phone1');
                sessionStorage.removeItem('email');
                sessionStorage.removeItem('line');
                console.log('Cancel');
            },
        });
    };


    saveOk = (e) => {
        e.preventDefault();
        this.formRef.props.form.validateFields(async (err, values) => {
            if (!err) {
                console.log('value', values)
                console.log('no err');
                await this.addSave();
                this.setState({ loading: true });
      
            } else {
                console.log('err');
                console.log('value', values)
            };

        });
    };

    addSave = async () => {
        let FieldsValue = this.formRef.props.form.getFieldsValue();
        console.log(FieldsValue)
        let trackContactDate = await convertDate(FieldsValue.trackContactDate._d);
        let trackNextContactDate = await convertDate(FieldsValue.trackNextContactDate._d);
        let trackTestBirthday;
        if (FieldsValue.trackTestBirthday !== undefined) {
            trackTestBirthday = await convertDate(FieldsValue.trackTestBirthday._d);
        }
        let isGoogle;
        if (FieldsValue.isGoogle !== undefined) {
            isGoogle = FieldsValue.isGoogle.length > 0 ? "true" : "false";
        }

        var customer_id_QR = "";
        if (sessionStorage.getItem('newQR_id') !== undefined)
            customer_id_QR = sessionStorage.getItem('newQR_id');

        let obj = {
            name: sessionStorage.name,
            gender: sessionStorage.gender,
            come_date: sessionStorage.come_date,
            type: sessionStorage.type,
            event_info: sessionStorage.event_info,
            event_place: sessionStorage.event_place,
            event_date: sessionStorage.event_date,
            model1: sessionStorage.model1,
            phone1: sessionStorage.phone1,
            email: sessionStorage.email,
            line: sessionStorage.line,
            state_m: 'M',
            state_t: 'T',
            state_p: 'P',
            state: document.getElementById("state").innerHTML,//page2 購車熱度
            customer_id_QR: customer_id_QR
        };
        console.log(obj);
        let ajaxResult = await ajaxApi(obj, "potential/create");
        console.log(ajaxResult)
        setTimeout(async () => {
            if (ajaxResult != null) {
                var cid = ajaxResult.id
                var msg = ajaxResult.msg
                if (cid !== undefined) {
                    obj = {
                        id: cid,
                        state_m: 'M',
                        state_t: 'T',
                        state_p: 'P',
                        state: document.getElementById("state").innerHTML,//page2 購車熱度
                        contact_date: trackContactDate,//page2 本次接觸日期
                        contact_site: FieldsValue.trackContactSite,//page2 本次接觸地點
                        contact_type: FieldsValue.trackContactType,//page2 接觸事項
                        test_model: FieldsValue.trackTestModel === undefined ? '' : FieldsValue.trackTestModel,//page2 試車車型
                        test_id: '',
                        test_birthday: trackTestBirthday === undefined ? '' : trackTestBirthday,//page2 試車人生日
                        test_address: FieldsValue.trackTestAddress === undefined ? '' : FieldsValue.trackTestAddress,//page2 試車人地址
                        test_phone: FieldsValue.trackTestPhone === undefined ? '' : FieldsValue.trackTestPhone.replace(/-/g, ''),//page2 試車人電話
                        next_contact_date: trackNextContactDate,//page2 下次接觸日期
                        next_contact_point: FieldsValue.trackNextContactPoint,//page2 下次接觸事項
                        remark: FieldsValue.trackRemarks === undefined ? '' : FieldsValue.trackRemarks,//page2 備註
                        isGoogle: isGoogle === undefined ? false : isGoogle
                    };
                    console.log(obj)
                    let Result = await ajaxApi(obj, "potential/createtrack");
                    console.log(Result)

                    if (Result != null) {
                        this.success(msg);
                        sessionStorage.setItem('customer_id', cid);
                        sessionStorage.removeItem("newQR_id");
                        sessionStorage.removeItem('name');
                        sessionStorage.removeItem('gender');
                        sessionStorage.removeItem('come_date')
                        sessionStorage.removeItem('type');
                        sessionStorage.removeItem('event_info');
                        sessionStorage.removeItem('event_place');
                        sessionStorage.removeItem('event_date');
                        sessionStorage.removeItem('model1');
                        sessionStorage.removeItem('phone1');
                        sessionStorage.removeItem('email');
                        sessionStorage.removeItem('line');
                        window.location = '/SMT/Front/PotentialClient/Edit.html';
                        setTimeout(() => {
                            this.setState({ loading: false, visible: false });
                          }, 3000);
          
                    };
                };
            };
        }, 1000);
    };

    success = (msg) => {
        Modal.success({
            title: msg,
            content: '',
        });
    }

    handleOk = async (e) => {
        e.preventDefault();
        let formVals = this.form.props.form.getFieldsValue();

        console.log(formVals)
        let formErr = !formVals.addPhone1 && !formVals.addEmail && !formVals.addLine
        if (formErr) {
            this.setState({
                validateStatus: "warning"
            });
        } else {
            this.setState({
                validateStatus: ""
            });
        };
        this.form.props.form.validateFields(async (err, values) => {

            if (!err && !formErr) {
                if (formVals.addEmail) {
                    if (!formVals.addEmail.includes("@")) {
                        alert('EMAIL格式錯誤');
                    } else {
                        await this.doubleCheck(values, formVals);
                    };
                } else {
                    await this.doubleCheck(values, formVals);
                };
            };
        });
    };



    doubleCheck = async (values, formVals) => {

        come_date = await convertDate(formVals.come_date._d);

        if (formVals.event_date !== undefined) {
            event_date = await convertDate(formVals.event_date._d);
        }
        console.log(event_date)
        console.log(moment().format("YYYY-MM-DD"))
        let obj = {
            name: values.addName,
            phone1: 'M' + values.addPhone1.replace(/\-/g, ''),
        };
        let data = await ajaxApi(obj, "potential/doublecheck");
        if (data.msg === true) {
            sessionStorage.setItem('name', formVals.addName);
            sessionStorage.setItem('gender', formVals.modifier);
            sessionStorage.setItem('come_date', come_date)
            sessionStorage.setItem('type', formVals.addType);
            sessionStorage.setItem('event_info', formVals.addIfo === undefined ? '' : formVals.addIfo);
            sessionStorage.setItem('event_place', formVals.addEventPlace === undefined ? '' : formVals.addEventPlace);
            sessionStorage.setItem('event_date', formVals.event_date === undefined ? '' : event_date);
            sessionStorage.setItem('model1', formVals.addModel1);
            sessionStorage.setItem('phone1', formVals.addPhone1 === undefined ? '' : formVals.addPhone1);
            sessionStorage.setItem('email', formVals.addEmail === undefined ? '' : formVals.addEmail);
            sessionStorage.setItem('line', formVals.addLine === undefined ? '' : formVals.addLine);
            this.showConfirm();
            setTimeout(() => {
                this.setState({ loading: false, visible: false });
            }, 50);
        } else {
            this.setState({
                visible: 2,
            });
            sessionStorage.setItem('name', formVals.addName);
            sessionStorage.setItem('gender', formVals.modifier);
            sessionStorage.setItem('come_date', come_date)
            sessionStorage.setItem('type', formVals.addType);
            sessionStorage.setItem('event_info', formVals.addIfo === undefined ? '' : formVals.addIfo);
            sessionStorage.setItem('event_place', formVals.addEventPlace === undefined ? '' : formVals.addEventPlace);
            sessionStorage.setItem('event_date', formVals.event_date === undefined ? '' : event_date);
            sessionStorage.setItem('model1', formVals.addModel1);
            sessionStorage.setItem('phone1', formVals.addPhone1 === undefined ? '' : formVals.addPhone1);
            sessionStorage.setItem('email', formVals.addEmail === undefined ? '' : formVals.addEmail);
            sessionStorage.setItem('line', formVals.addLine === undefined ? '' : formVals.addLine);
        };
    };



    handleCancel = () => {
        this.setState({ visible: false });
    };


    onChange = e => {
        console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });
    };



    render() {
        const { visible } = this.state;

        return (
            <div id="collapseBox">
                <Button type="primary" className="addbtn btn-info" onClick={this.showModal}>
                    <Icon type="plus" style={{ fontSize: '20px' }} />
                </Button>

                <Modal className="modelClient"
                    width={900}
                    visible={visible === 1}
                    title="新增潛在客戶"
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    destroyOnClose={true}
                    footer={[
                        <Button key="submit" htmlType="submit" type="primary" onClick={this.handleOk}>
                            下一步
                            </Button>,
                        <Button key="back" onClick={this.handleCancel}>
                            取消
                             </Button>,
                    ]}>
                    <WrappedTimeRelatedForm wrappedComponentRef={(form) => this.form = form} validateStatus={this.state.validateStatus} />
                </Modal>
                <Modal className="modelClient"
                    width={900}
                    visible={visible === 2}
                    title="新增接觸活動"
                    onOk={this.saveOk}
                    onCancel={this.handleCancel}
                    destroyOnClose={true}
                    footer={[
                        <Button key="submit" htmlType="submit" type="primary" onClick={this.saveOk.bind(this)}>
                            儲存
                            </Button>,
                        <Button key="back" onClick={this.handleCancel}>
                            取消
                             </Button>,
                    ]}>
                    <PotentialClient wrappedComponentRef={(inst) => this.formRef = inst} />
                </Modal>
            </div>
        );
    }
}

export default ModelBtn;
