import React from 'react';
import Dropdown from '../component/Dropdown';
import sidebar from '../json/sidebar.json';
class Sidebar extends React.Component {


    render() {
        return (
            <div id="side_bar">
                <div className="logo">
                    <div>Sales Tool</div>
                    <div className="version">V3.2.14</div>
                </div>
                <Dropdown data={sidebar}/>
            </div>
        );
    }
}


export default Sidebar;