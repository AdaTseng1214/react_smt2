import React, { Component } from 'react';
import {
    Row,
    Col,
    Collapse,
    Icon,
    Card,
    Upload,
    Modal,
    Button,
    Select

} from "antd";

import { errorModel, success, getBase64 } from '../function/function';
import { ajaxApi, writeTab } from '../function/Api';
const { Panel } = Collapse;
const { Option } = Select;
const customPanelStyle = {
    background: '#f7f7f7',
    borderRadius: 4,
    marginBottom: 24,
    border: '1px solid #ccc',
    overflow: 'hidden',
};
const customer_id = sessionStorage.customer_id;

const renderImg = [
    {
        uid: '-1',
        name: 'xxx.png',
        status: 'done',
        url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
    },
]
class PhotoMaker extends Component {
    state = {
        previewVisible: false,
        previewImage: '',
        fileList: renderImg,
        SelectVal: '',
        ImgData: [],
    };

    handleCancel = () => this.setState({ previewVisible: false });

    handlePreview = async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }

        this.setState({
            previewImage: file.url || file.preview,
            previewVisible: true,
        });
    };

    handleSelect = (value) => {
        this.setState({
            SelectVal: value
        });
        console.log(this.state.SelectVal)
    };

    handleChange = ({ fileList }) => {

        this.setState({ fileList });
    };

    enterLoading = () => {

        let valueType = this.state.SelectVal;
        if (!valueType) {
            errorModel('請選擇檔案類型');
        } else {
            this.updated();
        }
    };


    /**
     * 上傳照片
     */
    updated = async () => {
        let obj = {
            id: customer_id,
            type: this.state.SelectVal,
            filedata: this.state.fileList[0].thumbUrl
        };
        console.log(obj)
        let data = await ajaxApi(obj, "potential/createfile");
        if (data != null) {
            await success(data);
            let ImgData = await writeTab();
            console.log(ImgData);
        };
    };
    render() {
        const { previewVisible, previewImage, fileList } = this.state;
        const Img = this.props.data;


        const uploadButton = (
            <div>
                <Icon type="plus" />
                <div className="ant-upload-text">新增照片</div>
            </div>
        );
        return (
            <>

                <Collapse
                    bordered={false}
                    expandIcon={({ isActive }) => <Icon type={isActive ? "zoom-out" : "zoom-in"} />}>
                    <Panel header="照片管理" key="" style={customPanelStyle}>

                        <Card>
                            <Row>
                                <Col className="text-secondary"
                                    xs={7}
                                    lg={7}
                                    md={7}
                                    sm={7}>
                                    <div className="clearfix">
                                        <Upload
                                            listType="picture-card"
                                            fileList={fileList}
                                            onPreview={this.handlePreview}
                                            onChange={this.handleChange}>
                                            {fileList.length >= 2 ? null : uploadButton}
                                        </Upload>
                                        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                                            <img alt="example" style={{ width: '100%' }} src={previewImage} />
                                        </Modal>
                                    </div>
                                    <div>
                                        {this.props.data.length === 0 ? <Select placeholder="請選擇" style={{ width: 90 }} onChange={this.handleSelect}>
                                            <Option value="00">其他</Option>
                                            <Option value="01">駕照</Option>
                                        </Select> : null}
                                        {this.props.data.length === 0 ? <Button type="primary" loading={this.state.loading} onClick={this.enterLoading}>
                                            <Icon type="upload" />上傳照片
                                        </Button> : null}
                                        <Button type="danger" loading={this.state.loading} onClick={this.enterLoading}>
                                            <Icon type="delete" />刪除照片
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </Card>
                    </Panel>
                </Collapse>
            </>
        );
    };
};

export default PhotoMaker;