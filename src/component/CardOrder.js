import React, { Component } from 'react';
import { Collapse, Card, Col, Row, Pagination, Table, Icon } from 'antd';
import { ajaxApi, getOrderDetail } from '../function/Api';
import { pageTotal } from '../function/function';
import { Empty } from 'antd';

const Panel = Collapse.Panel;
const columns = [
    {
        title: '',
        dataIndex: 'contract_date',
    },
    {
        title: 'Action',
        key: 'customer_id',
        render: () => <a href="javascript:;"><Icon type="form" /></a>,
    },
];

class CardOrder extends Component {
    state = {
        open: true,
        sps_nameA: "",
        sps_nameB: "",
        sps_nameC: "",
        sps_nameD: "",
        sps_nameE: "",
        sps_nameF: "",
        sps_nameG: "",
        sps_nameH: "",
        scountA: "",
        scountB: "",
        scountC: "",
        scountD: "",
        scountE: "",
        scountF: "",
        scountG: "",
        scountH: "",
        ReviewTotal: 0,
        cancelTotal: 0,
        DispatchTotal: 0,
        ListingTotal: 0,
        DeliveryTotal: 0,
        OverruleTotal: 0,
        ReservedTotal: 0,
        UnpaidTotal: 0,
        Dispatch: [],
        cancel: [],
        Review: [],
        Listing: [],
        Delivery: [],
        Overrule: [],
        Reserved: [],
        Unpaid: [],
    };
    async componentDidMount() {
        let APIdata = await ajaxApi({}, "frontindex/searchorderstatecount")
        console.log(APIdata)

        this.setState({
            sps_nameA: APIdata.data[0].sps_name,
            sps_nameB: APIdata.data[1].sps_name,
            sps_nameC: APIdata.data[2].sps_name,
            sps_nameD: APIdata.data[3].sps_name,
            sps_nameE: APIdata.data[4].sps_name,
            sps_nameF: APIdata.data[5].sps_name,
            sps_nameG: APIdata.data[6].sps_name,
            sps_nameH: APIdata.data[7].sps_name,
            scountA: APIdata.data[0].scount,
            scountB: APIdata.data[1].scount,
            scountC: APIdata.data[2].scount,
            scountD: APIdata.data[3].scount,
            scountE: APIdata.data[4].scount,
            scountF: APIdata.data[5].scount,
            scountG: APIdata.data[6].scount,
            scountH: APIdata.data[7].scount,
        });
    }

    callback(key) {
        console.log(key);
    };

    Collapse = async () => {
        let Review = await getOrderDetail(1, this.state.sps_nameA);
        let ReviewTotal = await pageTotal(Review.pagecount);
        console.log(Review.data.length !== 0)
        this.setState({
            Review: Review.data,
            ReviewTotal: ReviewTotal,
        });
    };

    CollapseDispatch = async () => {
        let Dispatch = await getOrderDetail(1, this.state.sps_nameB);
        let DispatchTotal = await pageTotal(Dispatch.pagecount);

        this.setState({
            Dispatch: Dispatch.data,
            DispatchTotal: DispatchTotal,
        });
    };

    CollapseListing = async () => {
        let Listing = await getOrderDetail(1, this.state.sps_nameC);
        let ListingTotal = await pageTotal(Listing.pagecount);
        this.setState({
            Listing: Listing.data,
            ListingTotal: ListingTotal,
        });
    };

    CollapseUnpaid = async () => {
        let Unpaid = await getOrderDetail(1, this.state.sps_nameD);
        let UnpaidTotal = await pageTotal(Unpaid.pagecount);
        console.log(Unpaid.data.length !== 0)
        this.setState({
            Unpaid: Unpaid.data,
            UnpaidTotal: UnpaidTotal,
        });
    };

    CollapseDelivery = async () => {
        let Delivery = await getOrderDetail(1, this.state.sps_nameE);
        let DeliveryTotal = await pageTotal(Delivery.pagecount);
        this.setState({
            Delivery: Delivery.data,
            DeliveryTotal: DeliveryTotal,
        });
    };

    CollapseOverrule = async () => {
        let Overrule = await getOrderDetail(1, this.state.sps_nameF);
        let OverruleTotal = await pageTotal(Overrule.pagecount);
        this.setState({
            Overrule: Overrule.data,
            OverruleTotal: OverruleTotal,
        });
    };

    CollapseReserved = async () => {
        let Reserved = await getOrderDetail(1, this.state.sps_nameG);
        let ReservedTotal = await pageTotal(Reserved.pagecount);
        this.setState({
            Reserved: Reserved.data,
            ReservedTotal: ReservedTotal,
        });
    };

    CollapseCancel = async () => {
        let cancel = await getOrderDetail(1, this.state.sps_nameH);
        console.log(cancel)
        console.log(cancel.data.length !== 0)
        let cancelTotal = await pageTotal(cancel.pagecount);

        this.setState({
            cancel: cancel.data,
            cancelTotal: cancelTotal,
        });
    };

    onChangePage = async page => {
        let cancel = await getOrderDetail(1, this.state.sps_nameH);
        let Review = await getOrderDetail(1, this.state.sps_nameA);

        this.setState({
            current: page,
            cancel: cancel.data,
            Review: Review.data,
        });
    };

    render() {
        const { Review, cancel, Dispatch, Listing, Delivery, Unpaid, Overrule, Reserved, cancelTotal, ReviewTotal, DispatchTotal, ListingTotal, DeliveryTotal, UnpaidTotal, OverruleTotal, ReservedTotal } = this.state;
        console.log(cancel)
        return (
            <Collapse defaultActiveKey={['1']} onChange={this.callback}>
                <Panel className="text-center" header="訂單" key="1">
                    <Row gutter={16}>
                        <Col className="text-center" span={12}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{this.state.scountA}</h1>
                                <p>{this.state.sps_nameA}</p>
                                <Collapse
                                    className="target-header" onChange={this.Collapse}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="Review">
                                        {Review.length !== 0 ? <Table columns={columns}
                                            showHeader={false}
                                            rowKey={Review => Review.customer_id}
                                            dataSource={Review}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" /> : <Empty imageStyle={{ height: 60, }} description={<span>尚無資料</span>}></Empty>}
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={ReviewTotal} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                        <Col className="text-center" span={12}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{this.state.scountB}</h1>
                                <p>{this.state.sps_nameB}</p>
                                <Collapse
                                    className="target-header" onChange={this.CollapseDispatch}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="Dispatch">
                                        {Dispatch.length !== 0 ? <Table columns={columns}
                                            showHeader={false}
                                            rowKey={Dispatch => Dispatch.customer_id}
                                            dataSource={Dispatch}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" /> : <Empty imageStyle={{ height: 60, }} description={<span>尚無資料</span>}></Empty>}
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={DispatchTotal} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                        <Col className="text-center" span={12}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{this.state.scountC}</h1>
                                <p>{this.state.sps_nameC}</p>
                                <Collapse
                                    className="target-header" onChange={this.CollapseListing}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="Listing">
                                        {Listing.length !== 0 ? <Table columns={columns}
                                            showHeader={false}
                                            rowKey={Listing => Listing.customer_id}
                                            dataSource={Listing}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" /> : <Empty imageStyle={{ height: 60, }} description={<span>尚無資料</span>}></Empty>}
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={ListingTotal} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                        <Col className="text-center" span={12}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{this.state.scountD}</h1>
                                <p>{this.state.sps_nameD}</p>
                                <Collapse
                                    className="target-header" onChange={this.CollapseUnpaid}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="Unpaid">
                                        {Unpaid.length !== 0 ? <Table columns={columns}
                                            showHeader={false}
                                            rowKey={Unpaid => Unpaid.customer_id}
                                            dataSource={Unpaid}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" /> : <Empty imageStyle={{ height: 60, }} description={<span>尚無資料</span>}></Empty>}
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={UnpaidTotal} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                        <Col className="text-center" span={12}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{this.state.scountE}</h1>
                                <p>{this.state.sps_nameE}</p>
                                <Collapse
                                    className="target-header" onChange={this.CollapseDelivery}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="Delivery">
                                        {Delivery.length !== 0 ? <Table columns={columns}
                                            showHeader={false}
                                            rowKey={Delivery => Delivery.customer_id}
                                            dataSource={Delivery}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" /> : <Empty imageStyle={{ height: 60, }} description={<span>尚無資料</span>}></Empty>}
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={DeliveryTotal} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                        <Col className="text-center" span={12}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{this.state.scountF}</h1>
                                <p>{this.state.sps_nameF}</p>
                                <Collapse
                                    className="target-header" onChange={this.CollapseOverrule}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="yesterday">
                                        {Overrule.length !== 0 ? <Table columns={columns}
                                            showHeader={false}
                                            rowKey={Overrule => Overrule.customer_id}
                                            dataSource={Overrule}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" /> : <Empty imageStyle={{ height: 60, }} description={<span>尚無資料</span>}></Empty>}
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={OverruleTotal} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                        <Col className="text-center" span={12}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{this.state.scountG}</h1>
                                <p>{this.state.sps_nameG}</p>
                                <Collapse
                                    className="target-header" onChange={this.CollapseReserved}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="yesterday">
                                        {Reserved.length !== 0 ? <Table columns={columns}
                                            showHeader={false}
                                            rowKey={Reserved => Reserved.customer_id}
                                            dataSource={Reserved}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" /> : <Empty imageStyle={{ height: 60, }} description={<span>尚無資料</span>}></Empty>}
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={ReservedTotal} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                        <Col className="text-center" span={12}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{this.state.scountH}</h1>
                                <p>{this.state.sps_nameH}</p>
                                <Collapse
                                    className="target-header" onChange={this.CollapseCancel}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="yesterday">
                                        {cancel.length !== 0 ? <Table columns={columns}
                                            showHeader={false}
                                            rowKey={cancel => cancel.customer_id}
                                            dataSource={cancel}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" /> : <Empty imageStyle={{ height: 60, }} description={<span>尚無資料</span>}></Empty>}
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={cancelTotal} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                    </Row>
                </Panel>
            </Collapse>
        );
    }
}

export default CardOrder;