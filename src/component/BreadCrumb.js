import React, { Component } from 'react'
import { Breadcrumb,} from "antd";
 class BreadCrumb extends Component {
    render() {
        return (
            <>
                <Breadcrumb className="bg-light">
                    <Breadcrumb.Item><h3 className="pageName">潛在客戶管理 - 修改潛在客戶資料</h3></Breadcrumb.Item>
                </Breadcrumb>
                <Breadcrumb>
                    <Breadcrumb.Item>{this.props.data.name}</Breadcrumb.Item>
                    <Breadcrumb.Item>{this.props.data.state}</Breadcrumb.Item>
                    <Breadcrumb.Item>{this.props.data.car_model1}</Breadcrumb.Item>
                </Breadcrumb>
            </>
        )
    };
};
export default BreadCrumb;