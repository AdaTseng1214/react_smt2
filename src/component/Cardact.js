import React, { Component } from 'react';
import { Collapse, Card, Col, Row, Table, Pagination, Icon } from 'antd';
import { ajaxApi, getTrackDate, getTrackCount } from '../function/Api';
import { pageTotal, convertDate } from '../function/function';


const Panel = Collapse.Panel;
const columns = [
    {
        title: '',
        dataIndex: 'car_model1',
    },
    {
        title: '',
        dataIndex: 'next_contact_point',
    },
    {
        title: '',
        dataIndex: 'name',
    },
    {
        title: '',
        dataIndex: 'next_contact_date',
    },
    {
        title: 'Action',
        key: 'customer_id',
        render: () => <a href="../PotentialClient/Edit.html"><Icon type="form" /></a>,
    },
];

const columnsTrial = [
    {
        title: '',
        dataIndex: 'customer_name',
    },
    {
        title: '',
        dataIndex: 'plate_no',
    },
    {
        title: '',
        dataIndex: 'time_trial',
    },
];



class Cardact extends Component {
    state = {
        scount: "",
        scountB: "",
        scountC: "",
        yesterday: [],
        today: [],
        trial: [],
        current: 1,
        ytotal: 0,
        todayTotal: 0,
    };

    getTab = async () => {
        sessionStorage.setItem('customer_id', this.state.yesterday);
        console.log(this.state.yesterday)
    }

    //今日預定試乘數
    getTrialDate = async (p) => {
        let date_trial = await convertDate(new Date());
        var obj = {
            date_trial: date_trial,
            plate_no: ''
        };
        console.log(obj)
        let ajaxResult = await ajaxApi(obj, "potentialtrial/searchbydate");

        if (ajaxResult != null) {
            let items = ajaxResult.data
            for (var i = 0; i < items.length; i++) {
                if (items[i].user_id === sessionStorage.userid) {
                    console.log(items)
                    return items;
                }
            };
        };

    };

    async componentDidMount() {
        let scount = await getTrackCount();

        this.setState({
            scount: scount.data[0].scount,
            scountB: scount.data[1].scount,
            scountC: scount.data[2].scount,
        });
    };

    CollapseTrial = async () => {
        let trial = await this.getTrialDate(1);

        this.setState({
            trial: trial,
        });
    };

    Collapse = async () => {
        let yesterday = await getTrackDate(1, 'yesterday');
        let ytotal = await pageTotal(yesterday.pagecount);

        this.setState({
            yesterday: yesterday.data,
            ytotal: ytotal,
        });

    };

    CollapseToday = async () => {
        let today = await getTrackDate(1, 'today');
        
        let todayTotal = await pageTotal(today.pagecount);
        this.setState({
            today: today.data,
            todayTotal: todayTotal,
        });
    };

    onChangePage = async page => {

        let yesterday = await getTrackDate(page, 'yesterday');
        this.setState({
            current: page,
            yesterday: yesterday.data,
        });
    };
    onChangetoday = async page => {

        let today = await getTrackDate(page, 'today');
        this.setState({
            current: page,
            today: today.data,
        });
    };

    callback(key) {
        console.log(key);
    };


    render() {
        const { scount, scountB, scountC, yesterday, today, ytotal, trial, todayTotal } = this.state;
        return (
            <Collapse defaultActiveKey={['1']} onChange={this.callback}>
                <Panel className="text-center" header="今日活動" key="1">
                    <Row gutter={16}>
                        <Col className="text-center" span={9}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{scount}</h1>
                                <p>累計至昨日未完成項目數</p>
                                <Collapse
                                    className="target-header" onChange={this.Collapse}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="yesterday">
                                        <Table
                                            onRow={record => {
                                                return {
                                                    onClick: event => {
                                                        sessionStorage.setItem('customer_id', record.customer_id)
                                                    }, // 点击行
                                                };
                                            }}
                                            columns={columns}
                                            showHeader={false}
                                            rowKey={yesterday => yesterday.customer_id}
                                            dataSource={yesterday}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" />

                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={ytotal} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                        <Col className="text-center" span={8}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{scountB}</h1>
                                <p>今日預定項目數</p>
                                <Collapse className="target-header" onChange={this.CollapseToday}>
                                    <Panel key="today">
                                        <Table columns={columns}
                                            showHeader={false}
                                            rowKey={today => today.customer_id}
                                            dataSource={today}
                                            pagination={false}

                                            size="middle" />
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangetoday} total={todayTotal} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                        <Col className="text-center" span={7}>
                            <Card bordered={false}>
                                <h1 className="target-num text-info">{scountC}</h1>
                                <p>今日預定試乘數</p>
                                <Collapse className="target-header" onChange={this.CollapseTrial}>
                                    <Panel key="trial">
                                        <Table columns={columnsTrial}
                                            showHeader={false}
                                            rowKey={trial => trial.id}
                                            dataSource={trial}
                                            pagination={false}
                                            size="middle" />
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={10} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                    </Row>
                </Panel>
            </Collapse>
        );
    };
};

export default Cardact;