import React, { Component } from 'react';
import BreadCrumbT from './BreadCrumbT';
import { Col, Row, Icon, Form, Select, Divider, Input } from 'antd';
import { ajaxApi, getCarDetailCodeMenu, getStockMenu, getMenu } from '../function/Api';
import SigningStock from '../model/SigningStock';

class StockList extends Component {

    state = {
        CarDetail: [],
        StockMenu: [],
        CarColor: [],
        SelectValues: '',
        YearVal: '',
        ColorVal: '',
    }

    handleChange = (value) => {
        let label = value.label
        this.setState({
            SelectValues: label
        })
    };
    changeYear = (val) => {
        this.setState({
            YearVal: val
        });
    };

    changeColor = (val) => {
        this.setState({
            ColorVal: val
        });

    };

    enter = (e) => {
        console.log(e)
    };

    InputVal = (e) => {
        console.log(e)
    };

    async componentDidMount() {
        let CarDetail = await getCarDetailCodeMenu();
        let StockMenu = await getStockMenu('year');
        let CarColor = await getMenu('col')
        this.setState({
            CarColor: CarColor.data,
            CarDetail: CarDetail.data,
            StockMenu: StockMenu.data
        });
    };

    focusText() {
        // this.textInput.focus();
        alert(this.textInput.value); //获取输入的值
    };

    render() {

        let { getFieldDecorator, setFieldsValue } = this.props.form;
        const { ColorVal, YearVal, CarDetail, StockMenu, CarColor, SelectValues } = this.state;

        const { Option } = Select;
        const formItem = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 12 }
            }
        };
        return (
            <>
                <div style={{ margin: '30px' }}>
                    <BreadCrumbT title="潛在客戶管理 - 簽訂 - 車輛庫存查詢" />
                    <p className="text-danger">(最後更新時間 : 2018-12-18 14:20:56)</p>
                </div>
                <div className="shadow-sm" style={{ background: '#f8f8f8', padding: '30px', margin: '30px' }}>
                    <Form {...formItem}>
                        <Row gutter={16}>
                            <h2 className="text-primary"><Icon type="search" /> 查詢</h2>
                            <Col xs={24} lg={8} md={8} sm={10}>
                                <Form.Item>
                                    {getFieldDecorator("searchModel", {
                                    })(
                                        <>
                                            <Select
                                                labelInValue={true}
                                                name="searchModel"
                                                style={{ width: 260 }}
                                                placeholder="請選擇訂購車型"
                                                optionFilterProp="children"
                                                onChange={this.handleChange}>
                                                {CarDetail.map((item, index) => (
                                                    <Option value={item.code} key={index}>
                                                        {item.model}
                                                    </Option>
                                                ))}
                                            </Select>
                                        </>
                                    )}
                                </Form.Item>

                            </Col>
                            <Col xs={24} lg={8} md={8} sm={8}>
                                <Form.Item>
                                    {getFieldDecorator("searchYear", {

                                    })(
                                        <Select
                                            name="searchYear"
                                            style={{ width: 260 }}
                                            placeholder="請選擇年式"
                                            optionFilterProp="children"
                                            onChange={this.changeYear}>
                                            {StockMenu.map((item, index) => (
                                                <Option value={item.year} key={index}>
                                                    {item.year}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={8} md={8} sm={5}>
                                <Form.Item>
                                    {getFieldDecorator("searchColor", {

                                    })(
                                        <Select
                                            name="editType"
                                            style={{ width: 260 }}
                                            placeholder="請選擇車色"
                                            optionFilterProp="children"
                                            onChange={this.handleChange}>
                                            {CarColor.map((item) => (
                                                <Option value={item.id} key={item.id}>
                                                    {item.sps_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                            {SelectValues || YearVal || ColorVal ? <Divider /> : null}
                            {SelectValues || YearVal || ColorVal ? <h3 className="text-primary"><Icon type="exclamation-circle" theme="filled" /> 查無庫存，請手動輸入車款：</h3> : null}
                            {SelectValues || YearVal || ColorVal ? <div className="mt-3">
                                <Col xs={24} lg={12} md={12} sm={12}>
                                    <Form.Item label="車型">
                                        {getFieldDecorator("searchModel", {
                                        })(
                                            <Select
                                                name="searchModel"
                                                style={{ width: 260 }}
                                                placeholder="請選擇訂購車型"
                                                optionFilterProp="children"
                                                onChange={this.changeSelect}>
                                                {CarDetail.map((item, index) => (
                                                    <Option value={item.code} key={index}>
                                                        {item.model}
                                                    </Option>
                                                ))}
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>


                                <Col xs={24} lg={12} md={12} sm={12}>
                                    <Form.Item label="年式">
                                        {getFieldDecorator("searchYear", {

                                        })(
                                            <Select
                                                name="searchYear"
                                                style={{ width: 260 }}
                                                placeholder="請選擇年式"
                                                optionFilterProp="children"
                                                onChange={this.changeYear}>
                                                {StockMenu.map((item, index) => (
                                                    <Option value={item.year} key={index}>
                                                        {item.year}
                                                    </Option>
                                                ))}
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>


                                <Col xs={24} lg={12} md={12} sm={12}>
                                    <Form.Item label="車色">
                                        {getFieldDecorator("searchColor", {

                                        })(
                                            <Select
                                                name="searchColor"
                                                style={{ width: 260 }}
                                                placeholder="請選擇車色"
                                                optionFilterProp="children"
                                                onChange={this.changeColor}>
                                                {CarColor.map((item) => (
                                                    <Option value={item.id} key={item.id}>
                                                        {item.sps_name}
                                                    </Option>
                                                ))}
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>


                                <Col xs={24} lg={12} md={12} sm={12}>
                                    <Form.Item label="內裝">
                                        {getFieldDecorator("noResultInterior", {
                                        })(
                                            <Input style={{ width: 260 }} name="noResultInterior" />
                                        )}
                                    </Form.Item>
                                </Col>

                                <Col xs={24} lg={12} md={12} sm={12}>
                                    <Form.Item label="特殊設備">
                                        {getFieldDecorator("noResultRemark", {
                                        })(
                                            <Input style={{ width: 260 }} name="noResultRemark" />
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12} md={12} sm={12}>
                                  
                                    <SigningStock Select={SelectValues} Color={ColorVal} Year={YearVal} />
                                </Col>
                            </div> : null}
                        </Row>
                    </Form>
                </div>
            </>
        );
    }
}

const StockListForm = Form.create()(StockList);
export default StockListForm;