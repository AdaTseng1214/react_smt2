import React, { Component } from 'react'
import {
    Form,
    Input,
    Button,
    Select
} from "antd";
import { relative } from "path";

const prefixSelector = (
    <Select style={{ width: 70 }}>
        <Select.Option value="M">手機</Select.Option>
        <Select.Option value="C">公司</Select.Option>
        <Select.Option value="H">住家</Select.Option>
    </Select>
);

class Contact extends Component {


    render() {
        let { getFieldDecorator } = this.props.form;

        return (
            <>
                <Form.Item
                    style={{display:'none'}}
                    className="editPhone d-flex"
                    label="聯絡方式"
                    style={{ position: relative }}>
                    {getFieldDecorator(this.props.id, {
                        initialValue: "21321321"
                    })(
                        <Input
                            addonBefore={getFieldDecorator(this.props.type, {
                                initialValue: "M"
                            })(prefixSelector)}
                            style={{ width: 240 }}
                        />
                    )}
                    <Button
                        type={this.props.buttonIcon === 'minus' ? 'primary' : 'danger'}
                        shape="circle"
                        icon={this.props.buttonIcon}
                        className={this.props.buttonIcon === 'minus' ? 'btnMinus' : ''}
                        // onClick={() => this.removePlus(this.props.Type)}
                        onClick={this.props.onBtnClick}
                    />
                </Form.Item>
            </>
        )
    };
};
const ContactForm = Form.create()(Contact);
export default ContactForm;