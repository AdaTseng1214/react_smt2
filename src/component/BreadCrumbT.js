import React, { Component } from 'react'
import { Breadcrumb} from 'antd';
export default class BreadCrumbT extends Component {
    render() {
        return (
            <>
                <Breadcrumb>
                    <Breadcrumb.Item>{this.props.title}</Breadcrumb.Item>
                </Breadcrumb>
            </>
        )
    }
}
