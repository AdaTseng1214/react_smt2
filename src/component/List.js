import React, { Component } from 'react';
import ModelBtn from '../model/ModelBtn';
import { Row, Col, Card, Input, Form } from 'antd';
import Sidebar from './Sidebar';
import BreadCrumbT from './BreadCrumbT';



class List extends Component {
    render() {
        let { getFieldDecorator } = this.props.form;
        return (
            <>
                <Row type="flex">
                    <div id="Accordion">
                        <BreadCrumbT title='潛在客戶管理'/>
                        <ModelBtn />
                        <div className="container-fluid">
                            <Card className="border-0 shadow-sm search-box">
                                <Row>
                                    <Col span={6}>
                                        <Input style={{ width: 265 }} size="large" placeholder="姓名" name="addName" />
                                        <Form.Item label="姓名">
                                            {getFieldDecorator('addName', {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: '請輸入姓名',
                                                    },
                                                ],
                                            })(<Input style={{ width: 265 }} size="large" placeholder="姓名" name="addName" />)}
                                        </Form.Item>
                                    </Col>
                                    <Col span={6}>
                                        <Form.Item>
                                            {getFieldDecorator('addName', {
                                            })(<Input style={{ width: 265 }} name="addName" />)}
                                        </Form.Item>
                                    </Col>
                                    <Col span={6}>
                                        <Form.Item label="姓名">
                                            {getFieldDecorator('addName', {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: '請輸入姓名',
                                                    },
                                                ],
                                            })(<Input style={{ width: 265 }} name="addName" />)}
                                        </Form.Item>
                                    </Col>
                                    <Form.Item label="姓名">
                                        {getFieldDecorator('addName', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: '請輸入姓名',
                                                },
                                            ],
                                        })(<Input style={{ width: 265 }} name="addName" />)}
                                    </Form.Item>
                                </Row>

                                <Row>
                                    <Form.Item>

                                    </Form.Item>
                                    <Form.Item label="電話" validateStatus={this.props.validateStatus}>
                                        {getFieldDecorator('addPhone1')(<Input style={{ width: 265 }} name="addPhone1" />)}
                                    </Form.Item>
                                    <Form.Item label="E-mail" validateStatus={this.props.validateStatus}>
                                        {getFieldDecorator('addEmail')(<Input style={{ width: 265 }} name="addEmail" />)}
                                    </Form.Item>
                                    <Form.Item label="@Line帳號" validateStatus={this.props.validateStatus}>
                                        {getFieldDecorator('addLine')(<Input style={{ width: 265 }} name="addLine" />)}
                                    </Form.Item>
                                </Row>

                            </Card>
                        </div>
                    </div>
                </Row>
            </>
        );
    };
};

const RelatedForm = Form.create()(List);
export default RelatedForm;