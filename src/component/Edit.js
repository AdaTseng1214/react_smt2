import React, { Component } from "react";
// import uuid from "react-uuid";
import {
    Row,
    Col,
    Form,
    Tabs,
    Input,
    Select,
    Modal,
    Collapse,
    Icon,
    Card,

} from "antd";
import BreadCrumb from '../component/BreadCrumb';

import PhotoMaker from '../component/PhotoMaker';
import TabSalesForm from '../Tab/TabSales';
import TabPurchaseForm from '../Tab/TabPurchase';
import TabImformationFrom from '../Tab/TabImformation';
import TabContactForm from '../Tab/TabContact';
import { ajaxApi,searchDetail, getMenu, getCityAreaMenu, getCarDetailMenu, getEventMenu, getCarDetailCodeMenu, googleCheckLink, googleCancel, writeTab, getList } from "../function/Api";
import { convertDate, convertYear, success, setNextContactDate } from '../function/function';

import moment from "moment";
import "moment/locale/zh-cn";

moment.locale("zh-cn");




const prefixSelector = (
    <Select style={{ width: 70 }}>
        <Select.Option value="M">手機</Select.Option>
        <Select.Option value="C">公司</Select.Option>
        <Select.Option value="H">住家</Select.Option>
    </Select>
);

const { Panel } = Collapse;
const { TabPane } = Tabs;
const { TextArea } = Input;
const dateFormat = "YYYY/MM/DD";
const confirm = Modal.confirm;

const customPanelStyle = {
    background: '#f7f7f7',
    borderRadius: 4,
    marginBottom: 24,
    border: '1px solid #ccc',
    overflow: 'hidden',
};


class Edit extends Component {
    state = {
        tabData: [],
        editType: [],
        editEventInfo: [],
        editOccupation: [],
        editExistingBrand: [],
        editModel1: [],
        editCompetition: [],
        editColor: [],
        editPaymentMethod: [],
        trackContactSite: [],
        trackContactType: [],
        getCarDetail: [],
        getEditPower: [],
        ListDetail: [],
        getCity: [],
        getArea: [],
        validateStatus: "",
        visible: false,
        isopen: false,
        hide: false,
        inputsArr: [],
        inputsC: 1,
        key: 3,
        TypeVal: '',
        carValue: '',
        editCity: '',
        editArea: '',
        address: '',
        address2: '',
        editCity2: '',
        editArea2: '',
        plusInput: [],
        editEventPlace: [],
        selectedEventMenu: [],
        nextContactType: [],
        getCarModel: [],
        text: '其他',
        nextDate: new Date(),
        TypeValue: '',
        renderImg: []
    };


    async componentDidMount() {
        let tabData= await searchDetail();
        this.setState({
            tabData:tabData.data[0]
        })
        this.getTab();
    }


    getTab = async () => {
        // this.searchDetail();
        let trackContactSite = await getMenu("tcs");
        let trackContactType = await getMenu("tct");
        let nextContactType = await getMenu('tct');
        let getCarModel = await getCarDetailCodeMenu();
        let ListDetail = await getList(1);
        this.setState({
            nextContactType: nextContactType.data,
            trackContactSite: trackContactSite.data,
            trackContactType: trackContactType.data,
            getCarModel: getCarModel.data,
            ListDetail: ListDetail.data
        });

    };

    /**
     * tab切換
     */
    callback = async (key) => {
        console.log(key)
        this.setState({
            key: key
        })
        switch (key) {
            case "1":
                // let editType = await getMenu("typ");
                // let editEventInfo = await getMenu("ifo");
                // let editOccupation = await getMenu("job");
                // let editExistingBrand = await getMenu("brd");
                // let getCity = await getCityAreaMenu("");
                // let getEvent = await getEventMenu();
                // let renderImg = await writeTab();

                // this.searchDetail();
                // this.setState({
                //     renderImg: renderImg.filedata,
                //     getCity: getCity.data,
                //     editType: editType.data,
                //     editEventInfo: editEventInfo.data,
                //     editOccupation: editOccupation.data,
                //     editExistingBrand: editExistingBrand.data,
                //     validateStatus: "",
                //     selectedEventMenu: getEvent.data
                // });
                break;
            case "2":
                // let editModel1 = await getCarMenu();
                // let editCompetition = await getMenu("brd");
                // let editColor = await getMenu("col");
                // let editPaymentMethod = await getMenu("pm");

                // this.searchDetail();
                // this.setState({
                //     editModel1: editModel1.data,
                //     editCompetition: editCompetition.data,
                //     editColor: editColor.data,
                //     editPaymentMethod: editPaymentMethod.data,

                // });
                break;

            case "3":
                // let trackContactSite = await getMenu("tcs");
                // let trackContactType = await getMenu("tct");
                // let nextContactType = await getMenu('tct');
                // let getCarModel = await getCarDetailCodeMenu();
                // this.searchDetail();
                // let ListDetail = await getList(1);
                // this.setState({
                //     ListDetail: ListDetail.data,
                //     nextContactType: nextContactType.data,
                //     trackContactSite: trackContactSite.data,
                //     trackContactType: trackContactType.data,
                //     getCarModel: getCarModel.data,
                // });
                break;
            default:
        }
    };

    handleChange = async value => {
        console.log(value);
    };



    onRegisteredChange = value => {
        this.setState({
            editArea2: value
        });
    }


    /**
     * model 戰敗
     */
    GameOver = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };






    purchasePlan = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log(values);
            if (!err) {
                console.log("Received values of form: ", values);
                this.editTab2Save();
            };
        });
    };



    /**
     * 購車計畫 UPDATE
     */
    editTab2Save = async () => {
        let values = this.props.form.getFieldsValue();
        console.log('這邊是', values);
        var obj = {
            id: sessionStorage.customer_id,
            car_model1: values.editModel1 === undefined ? '' : values.editModel1,
            car_powertrain: values.editPower === undefined ? '' : values.editPower,
            car_variants: values.editVariant === undefined ? '' : values.editVariant,
            car_competition: values.editCompetition === undefined ? '' : values.editCompetition,
            car_competition_model: values.editCompetitionModel === undefined ? '' : values.editCompetitionModel,
            car_color: values.editColor === undefined ? '' : values.editColor,
            payment_method: values.editPaymentMethod === undefined ? '' : values.editPaymentMethod
        };
        console.log(obj)
        let TabUp = await ajaxApi(obj, "potential/updatetab2");
        if (TabUp != null) {
            success('新增成功!');
        };
    };

  

    changeSelect = (val) => {
        console.log(val)
        this.setState({
            TypeVal: val
        })
    };






    plus = () => {
        if (this.state.inputsArr.length < 3) {

            if (this.state.inputsC > 4) {
                let sort = this.state.plusInput.sort();//排列
                this.setState({
                    inputsC: sort[0],
                });
                sort.shift();//刪除
            } else {
                var newInput = `editPhoneType${this.state.inputsC}`;
                console.log(newInput)
                this.setState(prevState => ({
                    inputsArr: prevState.inputsArr.concat([newInput]),
                    inputsC: prevState.inputsC + 1,
                }));
            };
        };
    };

    removePlus(item) {

        console.log('item', item)
        if (this.state.inputsArr.length > 0) {
            const list = this.state.inputsArr.filter(elm => elm !== item);
            console.log(list);
            const li = this.state.inputsArr.filter(elm => elm == item)//刪掉的那個數字
            let aa = String(li)
            let bb = aa.replace('editPhoneType', '')

            this.setState(prevState => ({
                inputsArr: list,
                // plusInput: prevState.plusInput.concat([li])
            }));
            console.log(list)
        };
    };

    onChange = (date) => {
        console.log(date);
    };

    ChangeEnter = (value) => {
        console.log(`selected ${value}`);
    };



    GoogleCheck = async (ckValues) => {
        console.log(ckValues)
        if (ckValues.length === 1) {
            googleCheckLink();
        } else {
            this.showConfirm();
        };
    };

    showConfirm = async () => {
        await confirm({
            title: '是否完全移除與 Google 帳號的連結?',
            okText: '確定',
            cancelText: '取消',
            onOk: async () => {
                googleCancel();
            },
            onCancel() {
                console.log('取消');
            },
        });
    };

    invalidConfirm = async () => {
        await confirm({
            title: '無效',
            content: '即將更改客戶狀態，是否確認無效？',
            okText: '確認無效',
            cancelText: '取消',
            onOk: async () => {

            },
            onCancel() {
                console.log('取消');
            },
        });
    };

    /**
     * 本次接觸事項
     */
    ChangeType = (TypeValue) => {
        this.setState({
            TypeValue: TypeValue
        });
    };

    /**
     * 購車熱度
     */
    onCheckbox = (ckValues) => {
        console.log(ckValues)
        if (ckValues.length === 3) {
            let nextDate = setNextContactDate(3);
            this.setState({
                text: 'H級',
                nextDate: nextDate
            });
        } else if (ckValues.length === 2 && !ckValues.includes("P")) {
            console.log(ckValues)
            let nextDate = setNextContactDate(7);
            this.setState({
                text: 'A級',
                nextDate: nextDate
            });
        } else if (ckValues.length === 2 && !ckValues.includes("T")) {
            let nextDate = setNextContactDate(30);
            this.setState({
                text: 'B級',
                nextDate: nextDate
            });
        } else if (ckValues.length === 1 && ckValues.includes("M")) {
            let nextDate = setNextContactDate(90);
            this.setState({
                text: 'C級',
                nextDate: nextDate,
                state_m: 'Y'
            });
        } else {
            if (ckValues.length === 1 && ckValues.includes("T")) {
                this.setState({
                    state_t: 'Y'
                });
            } else if (ckValues.length === 1 && ckValues.includes("P")) {
                this.setState({
                    state_t: 'Y'
                });
            }
            let nextDate = setNextContactDate(140);
            this.setState({
                text: '其他',
                nextDate: nextDate
            });
        };
    };

    render() {
        const {
            tabData,
            key,
            renderImg
        } = this.state;
console.log(tabData)

        console.log(renderImg)
        let phone = String(tabData.phone1);
        let phoneM = phone.replace(/[^\d]/g, '');

        let { getFieldDecorator, setFieldsValue } = this.props.form;
        const { Option } = Select;

        const formItem = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 12 }
            }
        };
        const config = {
            rules: [
                { type: "object", required: true, message: "Please select time!" }
            ]
        };
        return (
            <>
                <Row type="flex">
                    <div id="Accordion">
                        <BreadCrumb data={tabData} />
                        <div className="container-fluid mt-3">
                            <Tabs defaultActiveKey="3" onChange={this.callback}>
                                <TabPane tab="基本資料" key="1">
                                    {key == 1 ? <TabImformationFrom wrappedComponentRef={(form) => this.form = form}/> : null}
                                    <Col xs={24}
                                        lg={24}
                                        md={24}
                                        sm={24}>
                                        <PhotoMaker data={renderImg} />
                                    </Col>
                                </TabPane>


                                <TabPane tab="購車計畫" key="2">
                                    {key == 2 ? <TabPurchaseForm/> : null}
                                </TabPane>

                                <TabPane tab="接觸活動" key="3">
                                    {key == 3 ? <TabContactForm wrappedComponentRef={(inst) => this.formRef = inst}/> : null}
                                </TabPane>

                                <TabPane tab="銷售結果" key="4">
                                    {key == 4 ? <div style={{ padding: '15px' }}>
                                        <TabSalesForm  wrappedComponentRef={(form) => this.form = form}/>
                                    </div> : null}
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </Row>
            </>
        );
    }
}

const RelatedForm = Form.create()(Edit);
export default RelatedForm;
