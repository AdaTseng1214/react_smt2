import React, { Component } from 'react';
import { Menu, Icon } from 'antd';
import { NavLink, withRouter } from 'react-router-dom';

const SubMenu = Menu.SubMenu;
class Dropdown extends Component {

    // submenu keys of first level
    rootSubmenuKeys = ['1', '2', '3'];

    state = {
        openKeys: ['1'],
    };

    onOpenChange = openKeys => {
        const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
        if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
            this.setState({ openKeys });
        } else {
            this.setState({
                openKeys: latestOpenKey ? [latestOpenKey] : [],
            });
        };
    };
    render() {
        const JsData = JSON.stringify(this.props);
        const JsonData = JSON.parse(JsData)
        const usersElements = [];
        let Elements = [];
        console.log(JsonData)
        for (var i in JsonData.data) {
            Elements = [];
            if (JsonData.data[i].id !== "") {
                for (var k in JsonData.data[i].subnavItems) {
                    Elements.push(
                        <Menu.Item key={k}>
                            <NavLink to={JsonData.data[i].subnavItems[k].link}>
                                <span>{JsonData.data[i].subnavItems[k].title}</span>
                            </NavLink>
                        </Menu.Item>
                    )
                }
                usersElements.push(
                    <SubMenu key={i} title={<span><Icon type={JsonData.data[i].icon} /><span>{JsonData.data[i].title}</span></span>}>
                        {Elements}
                    </SubMenu>
                )
            } else {
                usersElements.push(<Menu.Item key={i}><Icon type={JsonData.data[i].icon} />{JsonData.data[i].title}
                    <NavLink to={JsonData.data[i].link}></NavLink>
                    </Menu.Item>)
            }
        }
        return (
            <Menu
                mode="inline"
                openKeys={this.state.openKeys}
                onOpenChange={this.onOpenChange}>
                {usersElements}
            </Menu>
        );
    }
}

export default Dropdown;