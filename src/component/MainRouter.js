import React, { Component } from 'react';
import Cardact from './Cardact'
import CardHA from './CardHA'
import CardOrder from './CardOrder'
import ModelBtn from '../model/ModelBtn';
import { Breadcrumb, Row, Col } from 'antd';
// import Sidebar from './Sidebar';



class AccordionOut extends Component {
  render = () => {
    return (
      <>
        <Row type="flex">
            <div id="Accordion">
              <Breadcrumb>
                <Breadcrumb.Item>首頁</Breadcrumb.Item>
              </Breadcrumb>
              <ModelBtn />
              <div className="container-fluid" id="accordionCard">
                <Cardact/>
                <CardHA/>
                <CardOrder/>
              </div>
            </div>
        </Row>
      </>
    );
  };
};

export default AccordionOut;




