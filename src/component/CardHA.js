import React, { Component } from 'react';
import { Collapse, Card, Col, Row, Icon, Pagination, Table } from 'antd';
import { ajaxApi, getStateDetail } from '../function/Api';
import { pageTotal } from '../function/function';


const Panel = Collapse.Panel;
const columns = [
    {
        title: '',
        dataIndex: 'name',
    },
    {
        title: '',
        dataIndex: 'car_model1',
    },
    {
        title: '',
        dataIndex: 'next_contact_point',
    },
    {
        title: 'Action',
        key: 'customer_id',
        render: () => <a href="javascript:;"><Icon type="form" /></a>,
    },
];

class CardHA extends Component {
    state = {
        scount: "",
        scountB: "",
        stateA: "",
        stateB: "",
        levelH: 0,
        levelA: 0,
        current: 1,
        HA: [],
        A: [],
    };
    async componentDidMount() {
        let scount = await ajaxApi({}, "frontindex/searchstatecount");
        // const scount = await this.props.ajaxApi;
        this.setState({
            scount: scount.data[0].scount,
            scountB: scount.data[1].scount,
            stateA: scount.data[0].state,
            stateB: scount.data[1].state
        });
    };
    Collapse = async () => {
        let HA = await getStateDetail(1, this.state.stateA);
        console.log(HA.data)
        let levelH = await pageTotal(HA.pagecount);

        this.setState({
            HA: HA.data,
            levelH: levelH
        });
    };

    CollapseLevelA = async () => {
        let A = await getStateDetail(1, this.state.stateB);
        console.log(A.data)
        let levelA = await pageTotal(A.pagecount);
        this.setState({
            A: A.data,
            levelA: levelA
        });
    };

    onChangePage = async page => {
        let HA = await getStateDetail(page, 'H級');
        this.setState({
            current: page,
            HA: HA.data,
        });
    };
    onChangeLevelA = async page => {
        let A = await getStateDetail(1, 'A級');
        this.setState({
            current: page,
            A: A.data,
        });
    };
    callback(key) {
        console.log(key);
    };

    render() {
        const { scount, scountB, stateA, stateB, levelH, HA, A, levelA } = this.state;
        return (
            <Collapse defaultActiveKey={['1']} onChange={this.callback}>
                <Panel className="text-center" header="H / A" key="1">
                    <Row gutter={16}>
                        <Col className="text-center" span={12}>
                            <Card bordered={false}>
                                <h1 className="target-num  text-info">{scount}</h1>
                                <p>{stateA}</p>
                                <Collapse
                                    className="target-header" onChange={this.Collapse}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="H級">
                                        <Table columns={columns}
                                            showHeader={false}
                                            rowKey={HA => HA.customer_id}
                                            dataSource={HA}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" />
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangePage} total={levelH} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                        <Col className="text-center" span={12}>
                            <Card bordered={false}>
                                <h1 className="target-num  text-info">{scountB}</h1>
                                <p>{stateB}</p>
                                <Collapse
                                    className="target-header" onChange={this.CollapseLevelA}
                                    expandIcon={({ isActive }) => <Icon type="right" rotate={isActive ? 90 : 0} />}>
                                    <Panel key="A級">
                                        <Table columns={columns}
                                            showHeader={false}
                                            rowKey={A => A.customer_id}
                                            dataSource={A}
                                            pagination={false}
                                            scroll={{ y: 240 }}
                                            size="middle" />
                                        <div className="Pagination">
                                            <Pagination size='small' hideOnSinglePage={true} current={this.state.current} onChange={this.onChangeLevelA} total={levelA} />
                                        </div>
                                    </Panel>
                                </Collapse>
                            </Card>
                        </Col>
                    </Row>
                </Panel>
            </Collapse>
        );
    };
};

export default CardHA;