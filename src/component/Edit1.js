import React, { Component } from "react";
// import uuid from "react-uuid";
import {
    Row,
    Col,
    Form,
    Tabs,
    Input,
    DatePicker,
    Select,
    Button,
    Radio,
    Checkbox,
    Modal,
    Collapse,
    Icon,
    Card,

} from "antd";
import BreadCrumb from '../component/BreadCrumb';
import ContactForm from '../component/Contact';
import PhotoMaker from '../component/PhotoMaker';
import TabSalesForm from '../Tab/TabSales';
import { ajaxApi, getMenu, getCarMenu, getCityAreaMenu, getCarDetailMenu, getEventMenu, getCarDetailCodeMenu, googleCheckLink, googleCancel, writeTab, getList } from "../function/Api";
import { convertDate, convertYear, success, setNextContactDate } from '../function/function';

import moment from "moment";
import "moment/locale/zh-cn";
import { relative } from "path";
moment.locale("zh-cn");




const prefixSelector = (
    <Select style={{ width: 70 }}>
        <Select.Option value="M">手機</Select.Option>
        <Select.Option value="C">公司</Select.Option>
        <Select.Option value="H">住家</Select.Option>
    </Select>
);

const { Panel } = Collapse;
const { TabPane } = Tabs;
const { TextArea } = Input;
const dateFormat = "YYYY/MM/DD";
const confirm = Modal.confirm;

const customPanelStyle = {
    background: '#f7f7f7',
    borderRadius: 4,
    marginBottom: 24,
    border: '1px solid #ccc',
    overflow: 'hidden',
};



class Edit extends Component {
    state = {
        tabData: [],
        editType: [],
        editEventInfo: [],
        editOccupation: [],
        editExistingBrand: [],
        editModel1: [],
        editCompetition: [],
        editColor: [],
        editPaymentMethod: [],
        trackContactSite: [],
        trackContactType: [],
        getCarDetail: [],
        getEditPower: [],
        ListDetail: [],
        getCity: [],
        getArea: [],
        validateStatus: "",
        visible: false,
        isopen: false,
        hide: false,
        inputsArr: [],
        inputsC: 1,
        key: 3,
        TypeVal: '',
        carValue: '',
        editCity: '',
        editArea: '',
        address: '',
        editCity2: '',
        editArea2: '',
        address2: '',
        plusInput: [],
        editEventPlace: [],
        selectedEventMenu: [],
        nextContactType: [],
        getCarModel: [],
        text: '其他',
        nextDate: new Date(),
        TypeValue: '',
        state_m: 'N',
        state_t: 'N',
        state_p: 'N',
        renderImg:[]
    };


    async componentDidMount() {
        this.getTab();
    }

    /**
     * Tab Detail
     */
    searchDetail = async () => {
        let tabData = await writeTab();
        if (!tabData.data[0].phone3 && !tabData.data[0].phone4) {
            this.setState({
                // inputsArr: ['editPhoneType','editPhoneType'],
                tabData: tabData.data[0],
            });
        } else if (!tabData.data[0].phone2 && !tabData.data[0].phone3 && !tabData.data[0].phone4) {
            console.log('2')
            this.setState({
                inputsArr: [],
                tabData: tabData.data[0],
            });
        } else if (!tabData.data[0].phone4) {
            console.log('第四個是空的');
            this.setState({
                inputsArr: ['editPhoneType', 'editPhoneType1', 'editPhoneType2'],
                tabData: tabData.data[0],
            });
        };
    };

    getTab = async () => {
        this.searchDetail();
        let trackContactSite = await getMenu("tcs");
        let trackContactType = await getMenu("tct");
        let nextContactType = await getMenu('tct');
        let getCarModel = await getCarDetailCodeMenu();
        let ListDetail = await getList(1);
        this.setState({
            nextContactType: nextContactType.data,
            trackContactSite: trackContactSite.data,
            trackContactType: trackContactType.data,
            getCarModel: getCarModel.data,
            ListDetail: ListDetail.data
        });

    };

    /**
     * tab切換
     */
    callback = async (key) => {
        this.setState({
            key: key
        })
        switch (key) {
            case "1":
                let editType = await getMenu("typ");
                let editEventInfo = await getMenu("ifo");
                let editOccupation = await getMenu("job");
                let editExistingBrand = await getMenu("brd");
                let getCity = await getCityAreaMenu("");
                let getEvent = await getEventMenu();
                let renderImg= await writeTab();

                this.searchDetail();
                this.setState({
                    renderImg:renderImg.filedata,
                    getCity: getCity.data,
                    editType: editType.data,
                    editEventInfo: editEventInfo.data,
                    editOccupation: editOccupation.data,
                    editExistingBrand: editExistingBrand.data,
                    validateStatus: "",
                    selectedEventMenu: getEvent.data
                });
                break;
            case "2":
                let editModel1 = await getCarMenu();
                let editCompetition = await getMenu("brd");
                let editColor = await getMenu("col");
                let editPaymentMethod = await getMenu("pm");

                this.searchDetail();
                this.setState({
                    editModel1: editModel1.data,
                    editCompetition: editCompetition.data,
                    editColor: editColor.data,
                    editPaymentMethod: editPaymentMethod.data,

                });
                break;

            case "3":
                let trackContactSite = await getMenu("tcs");
                let trackContactType = await getMenu("tct");
                let nextContactType = await getMenu('tct');
                let getCarModel = await getCarDetailCodeMenu();
                this.searchDetail();
                let ListDetail = await getList(1);
                this.setState({
                    ListDetail: ListDetail.data,
                    nextContactType: nextContactType.data,
                    trackContactSite: trackContactSite.data,
                    trackContactType: trackContactType.data,
                    getCarModel: getCarModel.data,
                });
                break;
            default:
        }
    };

    handleChange = async value => {
        console.log(value);
    };

    //聯絡地址
    handleProvinceChange = async city => {
        console.log(city);
        let getArea = await getCityAreaMenu(city);

        this.setState({
            getArea: getArea.data,
            editCity: city,
        });
    };

    //戶籍地址
    handleRegisteredChange = async city => {

        let getArea = await getCityAreaMenu(city);

        this.setState({
            getArea: getArea.data,
            editCity2: city
        });
    };

    onRegisteredChange = value => {
        this.setState({
            editArea2: value
        });
    }
    onSecondCityChange = value => {
        this.setState({
            secondCity: value,
            editArea: value,
        });
    };

    /**
     * model 戰敗
     */
    GameOver = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };




    handleSubmit = e => {
        e.preventDefault();


        this.props.form.validateFields((err, values) => {
            console.log('這邊是', values)
            let formErr =
                !values.editPhoneType && !values.editEmail && !values.editLine;

            if (formErr) {
                this.setState({
                    validateStatus: "warning"
                });
            } else {
                this.setState({
                    validateStatus: ""
                });
            }
            if (!err) {
                console.log("Received values of form: ", values);
                this.editTab1Save();

            };
        });
    };

    purchasePlan = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log(values);
            if (!err) {
                console.log("Received values of form: ", values);
                this.editTab2Save();
            };
        });
    };

    trackSave = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log(values);
            if (!err) {
                console.log("Received values of form: ", values);
                this.trackSaveTab();

            };
        });
    };

    trackSaveTab = async () => {
        let values = this.props.form.getFieldsValue();
        console.log(values)
        let trackContactDate = await convertDate(values.trackContactDate._d);
        let trackTestBirthday, isGoogle;
        if (values.trackTestBirthday !== undefined) {
            trackTestBirthday = await convertDate(values.trackTestBirthday._d);
        };

        if (values.isGoogle !== undefined) {
            isGoogle = values.isGoogle.length > 0 ? "true" : "false";
        }

        let trackNextContactDate = await convertDate(values.trackNextContactDate._d);
        var obj = {
            id: sessionStorage.customer_id,
            state_m: this.state.state_m,
            state_t: this.state.state_t,
            state_p: this.state.state_p,
            state: this.state.text,
            contact_date: trackContactDate,
            contact_site: values.trackContactSite,
            contact_type: values.trackContactType,
            test_model: values.trackContactSite,
            test_id: '',
            test_birthday: trackTestBirthday === undefined ? '' : values.trackTestBirthday,
            test_address: values.trackTestAddress === undefined ? '' : values.trackTestAddress,
            test_phone: values.trackTestPhone === undefined ? '' : values.trackTestPhone,
            next_contact_date: trackNextContactDate,
            next_contact_point: values.trackNextContactPoint,
            remark: values.trackRemarks === undefined ? '' : values.trackRemarks,
            isGoogle: isGoogle === undefined ? false : isGoogle
        };
        console.log(obj);
        let SaveTab = await ajaxApi(obj, "potential/createtrack");

        if (SaveTab.msg === "新增成功！") {
            success('新增成功!');
            await this.searchDetail();
            let ListDetail = await getList(1);
            this.setState({
                ListDetail: ListDetail.data,
            });
        };
    };

    /**
     * 購車計畫 UPDATE
     */
    editTab2Save = async () => {
        let values = this.props.form.getFieldsValue();
        console.log('這邊是', values);
        var obj = {
            id: sessionStorage.customer_id,
            car_model1: values.editModel1 === undefined ? '' : values.editModel1,
            car_powertrain: values.editPower === undefined ? '' : values.editPower,
            car_variants: values.editVariant === undefined ? '' : values.editVariant,
            car_competition: values.editCompetition === undefined ? '' : values.editCompetition,
            car_competition_model: values.editCompetitionModel === undefined ? '' : values.editCompetitionModel,
            car_color: values.editColor === undefined ? '' : values.editColor,
            payment_method: values.editPaymentMethod === undefined ? '' : values.editPaymentMethod
        };
        console.log(obj)
        let TabUp = await ajaxApi(obj, "potential/updatetab2");
        if (TabUp != null) {
            success('新增成功!');
        };
    };

    //save tab1
    editTab1Save = async () => {
        let values = this.props.form.getFieldsValue();
        let come_date = await convertDate(values.editComeDate._d);
        let editExistingYear, editEventDate, editBirthday;

        if (values.editBirthday !== undefined) {
            editBirthday = await convertDate(values.editBirthday._d);
        };
        if (values.editEventDate !== undefined) {
            editEventDate = await convertDate(values.editEventDate._d);
        };
        if (values.editExistingYear !== undefined) {
            editExistingYear = await convertYear(values.editExistingYear._d)
        };
        var obj = {
            id: sessionStorage.customer_id,
            name: values.editName === undefined ? '' : values.editName,
            gender: values.editSex,
            come_date: come_date,
            type: values.editType,
            event_info: values.editEventInfo === undefined ? '' : values.editEventInfo,
            event_place: values.editEventPlace === undefined ? '' : values.editIntroducer,
            event_date: editEventDate === undefined ? '' : editEventDate,
            Introducer: values.editIntroducer === undefined ? '' : values.editIntroducer, //介紹人
            license_plate: values.editLicense === undefined ? '' : values.editLicense,//現有客戶
            birthday: editBirthday === undefined ? '' : editBirthday,
            phone1: values.editPhoneType === undefined ? '' : values.PhoneType + values.editPhoneType,
            phone2: values.editPhoneType1 === undefined ? '' : values.PhoneType1 + values.editPhoneType1,
            phone3: values.editPhoneType2 === undefined ? '' : values.PhoneType2 + values.editPhoneType2,
            phone4: values.editPhoneType3 === undefined ? '' : values.PhoneType3 + values.editPhoneType3,
            occupation: values.editOccupation === undefined ? '' : values.editOccupation,//職業
            city: this.state.editCity,
            area: this.state.editArea,
            address: this.state.address,
            city2: this.state.editCity2, //戶籍地址
            area2: this.state.editArea2, //戶籍地址
            address2: this.state.address2, //戶籍地址
            email: values.editEmail === undefined ? '' : values.editEmail,
            line: values.editLine === undefined ? '' : values.editLine,
            existing_brand: values.editExistingBrand === undefined ? '' : values.editExistingBrand,  //現有車-品牌
            existing_model: values.editExistingModel === undefined ? '' : values.editExistingModel,
            existing_year: editExistingYear === undefined ? '' : editExistingYear,
            buy_car_reason: values.editBuyCarReason === undefined ? '' : values.editBuyCarReason,
            remark: values.editRemarks === undefined ? '' : values.editRemarks,
        };
        console.log(obj)
        let TabData = await ajaxApi(obj, "potential/updatetab1");

        if (TabData != null) {
            success('新增成功!');
        };

    };

    changeSelect = (val) => {
        console.log(val)
        this.setState({
            TypeVal: val
        })
    };

    handelInput = (e) => {
        this.setState({
            address: e.target.value,
            address2: e.target.value
        });
    };

    onChangecar = async (carValue) => {
        console.log(carValue)
        this.props.form.resetFields('editPower');
        let getCarDetail = await getCarDetailMenu(carValue, '', 'powertrain');
        this.setState({
            getCarDetail: getCarDetail.data,
            carValue: carValue,
        });

    };

    onChangePower = async (value) => {
        this.props.form.resetFields('editVariant');
        let getEditPower = await getCarDetailMenu(this.state.carValue, value, 'variants');
        this.setState({
            getEditPower: getEditPower.data
        });
    };

    // plus = () => {
    //     if (this.state.inputsC < 4) {
    //         this.setState({
    //             inputsC: this.state.inputsC + 1
    //         });
    //         console.log(this.state.inputsC + 1)
    //     };
    // };

    plus = () => {
        if (this.state.inputsArr.length < 3) {

            if (this.state.inputsC > 4) {
                let sort = this.state.plusInput.sort();//排列
                this.setState({
                    inputsC: sort[0],
                });
                sort.shift();//刪除
            } else {
                var newInput = `editPhoneType${this.state.inputsC}`;
                console.log(newInput)
                this.setState(prevState => ({
                    inputsArr: prevState.inputsArr.concat([newInput]),
                    inputsC: prevState.inputsC + 1,
                }));
            };
        };
    };

    removePlus(item) {

        console.log('item', item)
        if (this.state.inputsArr.length > 0) {
            const list = this.state.inputsArr.filter(elm => elm !== item);
            console.log(list);
            const li = this.state.inputsArr.filter(elm => elm == item)//刪掉的那個數字
            let aa = String(li)
            let bb = aa.replace('editPhoneType', '')

            this.setState(prevState => ({
                inputsArr: list,
                // plusInput: prevState.plusInput.concat([li])
            }));
            console.log(list)
        };
    };

    onChange = (date) => {
        console.log(date);
    };

    ChangeEnter = (value) => {
        console.log(`selected ${value}`);
    };



    GoogleCheck = async (ckValues) => {
        console.log(ckValues)
        if (ckValues.length === 1) {
            googleCheckLink();
        } else {
            this.showConfirm();
        };
    };

    showConfirm = async () => {
        await confirm({
            title: '是否完全移除與 Google 帳號的連結?',
            okText: '確定',
            cancelText: '取消',
            onOk: async () => {
                googleCancel();
            },
            onCancel() {
                console.log('取消');
            },
        });
    };

    invalidConfirm = async () => {
        await confirm({
            title: '無效',
            content: '即將更改客戶狀態，是否確認無效？',
            okText: '確認無效',
            cancelText: '取消',
            onOk: async () => {

            },
            onCancel() {
                console.log('取消');
            },
        });
    };

    /**
     * 本次接觸事項
     */
    ChangeType = (TypeValue) => {
        this.setState({
            TypeValue: TypeValue
        });
    };

    /**
     * 購車熱度
     */
    onCheckbox = (ckValues) => {
        console.log(ckValues)
        if (ckValues.length === 3) {
            let nextDate = setNextContactDate(3);
            this.setState({
                text: 'H級',
                nextDate: nextDate
            });
        } else if (ckValues.length === 2 && !ckValues.includes("P")) {
            console.log(ckValues)
            let nextDate = setNextContactDate(7);
            this.setState({
                text: 'A級',
                nextDate: nextDate
            });
        } else if (ckValues.length === 2 && !ckValues.includes("T")) {
            let nextDate = setNextContactDate(30);
            this.setState({
                text: 'B級',
                nextDate: nextDate
            });
        } else if (ckValues.length === 1 && ckValues.includes("M")) {
            let nextDate = setNextContactDate(90);
            this.setState({
                text: 'C級',
                nextDate: nextDate,
                state_m: 'Y'
            });
        } else {
            if (ckValues.length === 1 && ckValues.includes("T")) {
                this.setState({
                    state_t: 'Y'
                });
            } else if (ckValues.length === 1 && ckValues.includes("P")) {
                this.setState({
                    state_t: 'Y'
                });
            }
            let nextDate = setNextContactDate(140);
            this.setState({
                text: '其他',
                nextDate: nextDate
            });
        };
    };

    render() {
        const {
            isopen,
            visible,
            getCity,
            getArea,
            tabData,
            editType,
            editEventInfo,
            editOccupation,
            editExistingBrand,
            editModel1,
            editCompetition,
            editColor,
            editPaymentMethod,
            trackContactSite,
            trackContactType,
            nextContactType,
            key,
            getCarDetail,
            getEditPower,
            TypeVal,
            selectedEventMenu,
            text,
            nextDate,
            TypeValue,
            getCarModel,
            ListDetail,
            renderImg
        } = this.state;


        console.log(renderImg)
        let phone = String(tabData.phone1);
        let phoneM = phone.replace(/[^\d]/g, '');

        let { getFieldDecorator, setFieldsValue } = this.props.form;
        const { Option } = Select;



        const formItem = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 12 }
            }
        };
        const config = {
            rules: [
                { type: "object", required: true, message: "Please select time!" }
            ]
        };
        return (
            <>
                <Row type="flex">
                    <div id="Accordion">
                        <BreadCrumb data={tabData} />
                        <div className="container-fluid mt-3">
                            <Tabs defaultActiveKey="3" onChange={this.callback}>
                                <TabPane tab="基本資料" key="1">
                                    {key == 1 ? <Form {...formItem} className="tab-content" >
                                        <Row>
                                            <Col xs={24} lg={12} md={24} sm={24}>
                                                <Row>
                                                    <Form.Item label="姓名">
                                                        {getFieldDecorator("editName", {
                                                            initialValue: tabData.name,
                                                            rules: [
                                                                {
                                                                    required: true,
                                                                    message: "請輸入姓名"
                                                                }
                                                            ]
                                                        })(
                                                            <Input style={{ width: 265 }} name="editName" />
                                                        )}
                                                    </Form.Item>
                                                    <Form.Item label="性別">
                                                        {getFieldDecorator("editSex", {
                                                            initialValue: tabData.gender,
                                                            rules: [
                                                                {
                                                                    required: true,
                                                                    message: "請輸入姓名"
                                                                }
                                                            ]
                                                        })(
                                                            <Radio.Group>
                                                                <Radio value="M">男</Radio>
                                                                <Radio value="F">女</Radio>
                                                            </Radio.Group>
                                                        )}
                                                    </Form.Item>
                                                    <Form.Item label="來店日期" >
                                                        {getFieldDecorator("editComeDate", {
                                                            ...config,
                                                            initialValue: moment(tabData.come_date, dateFormat)
                                                        })(<DatePicker style={{ width: 265 }} />)}
                                                    </Form.Item>

                                                    <Form.Item label="客戶來源" hasFeedback>
                                                        {getFieldDecorator("editType", {
                                                            initialValue: tabData.type,
                                                            rules: [
                                                                { required: true, message: "請選擇客戶來源!" }
                                                            ]
                                                        })(
                                                            <Select
                                                                name="editType"
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇客戶來源"
                                                                optionFilterProp="children"
                                                                onChange={this.changeSelect}>
                                                                {editType.map((item, idex) => (
                                                                    <Option value={item.id} key={idex}>
                                                                        {item.sps_name}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>


                                                    {TypeVal == '001' || tabData.event_info !== "" ?
                                                        <Form.Item label="訊息來源" hasFeedback>
                                                            {getFieldDecorator("editEventInfo", {
                                                                initialValue: tabData.event_info,
                                                                rules: [
                                                                    { required: true, message: "請選擇訊息來源!" }
                                                                ]
                                                            })(
                                                                <Select
                                                                    name="editEventInfo"
                                                                    style={{ width: 265 }}
                                                                    placeholder="請選擇訊息來源"
                                                                    optionFilterProp="children">
                                                                    {editEventInfo.map((item, idex) => (
                                                                        <Option value={item.id} key={idex}>
                                                                            {item.sps_name}
                                                                        </Option>
                                                                    ))}
                                                                </Select>
                                                            )}
                                                        </Form.Item> : null}

                                                    {TypeVal == '004' ? <Form.Item label="介紹人">
                                                        {getFieldDecorator("editIntroducer", {
                                                        })(
                                                            <Input style={{ width: 265 }} name="editIntroducer" />
                                                        )}
                                                    </Form.Item> : null}

                                                    {TypeVal == '005' ? <Form.Item label="車牌(現有客戶)">
                                                        {getFieldDecorator("editLicense", {
                                                        })(
                                                            <Input style={{ width: 265 }} name="editLicense" />
                                                        )}
                                                    </Form.Item> : null}

                                                    {TypeVal == '003' ? <Form.Item label="外展地點" hasFeedback>
                                                        {getFieldDecorator("editEventPlace")(
                                                            <Select
                                                                name="editEventPlace"
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇活動"
                                                                optionFilterProp="children">
                                                                {selectedEventMenu.map((item) => (
                                                                    <Option value={item.name} key={item.name}>{item.name}</Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item> : null}

                                                    {TypeVal == '003' ? <Form.Item label="外展日期" hasFeedback>
                                                        {getFieldDecorator("editEventDate")(
                                                            <DatePicker
                                                                style={{ width: 265 }}
                                                                placeholder="請外展日期"
                                                            />
                                                        )}
                                                    </Form.Item> : null}

                                                    <Form.Item label="生日" hasFeedback>
                                                        {getFieldDecorator("editBirthday")(
                                                            <DatePicker
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇生日"
                                                            />
                                                        )}
                                                    </Form.Item>

                                                    <Form.Item label="職業" hasFeedback>
                                                        {getFieldDecorator("editOccupation")(
                                                            <Select
                                                                name="editOccupation"
                                                                showSearch
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇"
                                                                optionFilterProp="children"
                                                            >
                                                                {editOccupation.map((item, idex) => (
                                                                    <Option value={item.id} key={idex}>
                                                                        {item.sps_name}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>

                                                    <Form.Item
                                                        className="editPhone d-flex"
                                                        label="聯絡方式"
                                                        validateStatus={this.state.validateStatus}
                                                        style={{ position: relative }}>
                                                        {getFieldDecorator("editPhoneType", {
                                                            initialValue: phoneM
                                                        })(
                                                            <Input
                                                                addonBefore={getFieldDecorator('PhoneType', {
                                                                    initialValue: "M"
                                                                })(prefixSelector)}
                                                                style={{ width: 240 }}
                                                            />
                                                        )}
                                                        <Button
                                                            type="primary"
                                                            shape="circle"
                                                            icon="plus"
                                                            className="btnMinus"
                                                            onClick={this.plus}
                                                        />
                                                    </Form.Item>


                                                    {/* {inputsC > 1 ? <ContactForm id="editPhoneType1" Type="PhoneType1" /> : null}
                                                    {inputsC > 2 ? <ContactForm id="editPhoneType2" Type="PhoneType2" /> : null}
                                                    {inputsC > 3 ? <ContactForm id="editPhoneType3" Type="PhoneType3" /> : null} */}

                                                    {this.state.inputsArr.map((input, index) => {
                                                        console.log(input)
                                                        // let intValue;
                                                        // if (index > 0) {
                                                        //     intValue = tabData["phone" + (index + 1)].replace(/[^\d]/g, '')
                                                        // };
                                                        let intValue;

                                                        intValue = tabData["phone" + (index + 1)].replace(/[^\d]/g, '')

                                                        return (
                                                            <Form.Item
                                                                key={index}
                                                                label="聯絡方式"
                                                                validateStatus={this.state.validateStatus}
                                                                style={{ position: relative }}>
                                                                {getFieldDecorator(`editPhoneType${index + 1}`, {
                                                                    initialValue: intValue ? intValue : undefined
                                                                })(
                                                                    <Input
                                                                        onPressEnter={this.ChangeEnter}
                                                                        addonBefore={getFieldDecorator(`PhoneType${index + 1}`, {
                                                                            initialValue: "M"
                                                                        })(prefixSelector)}
                                                                        style={{ width: 240 }} />
                                                                )}
                                                                <Button
                                                                    type="danger"
                                                                    shape="circle"
                                                                    icon="minus"
                                                                    className="btnMinus"
                                                                    onClick={() => this.removePlus(input)}
                                                                />
                                                            </Form.Item>
                                                        )
                                                    })}



                                                    <Form.Item label="聯絡地址">
                                                        <Row className="d-flex" style={{ width: 265 }}>
                                                            <Select
                                                                placeholder="請選擇"
                                                                onChange={this.handleProvinceChange}>
                                                                {getCity.map((item, idex) => (
                                                                    <Option value={item.City} key={idex}>
                                                                        {item.City}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                            <Select
                                                                placeholder="請選擇"
                                                                onChange={this.onSecondCityChange}>
                                                                {getArea.map((item, idex) => (
                                                                    <Option value={item.Area} key={idex}>
                                                                        {item.Area}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        </Row>
                                                        <Input style={{ width: 265 }} name="addEmail" onChange={this.handelInput} />
                                                    </Form.Item>

                                                    <Form.Item label="戶籍地址">
                                                        <Row className="d-flex" style={{ width: 265 }}>
                                                            <Select
                                                                placeholder="請選擇"
                                                                onChange={this.handleRegisteredChange}>
                                                                {getCity.map((item, idex) => (
                                                                    <Option value={item.City} key={idex}>
                                                                        {item.City}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                            <Select
                                                                placeholder="請選擇"
                                                                onChange={this.onRegisteredChange}>
                                                                {getArea.map((item, idex) => (
                                                                    <Option value={item.Area} key={idex}>
                                                                        {item.Area}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        </Row>
                                                        <Input style={{ width: 265 }} name="addEmail" onChange={this.handelInput} />
                                                    </Form.Item>

                                                    <Form.Item
                                                        label="E-mail"
                                                        validateStatus={this.state.validateStatus}>
                                                        {getFieldDecorator("editEmail")(
                                                            <Input style={{ width: 265 }} name="editEmail" />
                                                        )}
                                                    </Form.Item>
                                                    <Form.Item
                                                        label="@Line帳號"
                                                        validateStatus={this.state.validateStatus}>
                                                        {getFieldDecorator("editLine")(
                                                            <Input style={{ width: 265 }} name="editLine" />
                                                        )}
                                                    </Form.Item>
                                                </Row>
                                            </Col>
                                            <Col xs={24} lg={12} md={24} sm={24}>
                                                <Row>
                                                    <Form.Item label="現有車-品牌" hasFeedback>
                                                        {getFieldDecorator("editExistingBrand")(
                                                            <Select
                                                                name="editExistingBrand"
                                                                showSearch
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇"
                                                                optionFilterProp="children">
                                                                {editExistingBrand.map((item, idex) => (
                                                                    <Option value={item.id} key={idex}>
                                                                        {item.sps_name}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>
                                                    <Form.Item label="現有車-車型">
                                                        {getFieldDecorator("editExistingModel")(
                                                            <Input style={{ width: 265 }} name="editExistingModel" />
                                                        )}
                                                    </Form.Item>

                                                    <Form.Item label="現有車-年式">
                                                        {getFieldDecorator("editExistingYear", {})(
                                                            <DatePicker
                                                                open={isopen}
                                                                mode="year"
                                                                placeholder="請選擇年份"
                                                                format="YYYY"
                                                                onOpenChange={status => {
                                                                    //弹出日历和关闭日历的回调
                                                                    console.log(status);
                                                                    if (status) {
                                                                        this.setState({ isopen: true });
                                                                    } else {
                                                                        this.setState({ isopen: false });
                                                                    }
                                                                }}
                                                                onPanelChange={v => {
                                                                    //日历面板切换的回调
                                                                    setFieldsValue({ editExistingYear: v });
                                                                    this.setState({
                                                                        isopen: false
                                                                    });
                                                                }}
                                                            />
                                                        )}
                                                    </Form.Item>
                                                    <Form.Item label="打算">
                                                        {getFieldDecorator("editBuyCarReason", {
                                                            initialValue: "增購"
                                                        })(
                                                            <Radio.Group>
                                                                <Radio value="增購">增購</Radio>
                                                                <Radio value="換購">換購</Radio>
                                                                <Radio value="首購">首購</Radio>
                                                            </Radio.Group>
                                                        )}
                                                    </Form.Item>

                                                    <Form.Item label="備註">
                                                        {getFieldDecorator("editRemarks", {})(
                                                            <TextArea rows={4} />
                                                        )}
                                                    </Form.Item>
                                                </Row>
                                            </Col>
                                        </Row>

                                        <Button type="primary" htmlType="submit" onClick={this.handleSubmit} block>
                                            儲存
                                     </Button>
                                    </Form> : null}

                                    <Col xs={24}
                                        lg={24}
                                        md={24}
                                        sm={24}>
                                        <PhotoMaker data={renderImg}/>
                                    </Col>
                                </TabPane>


                                <TabPane tab="購車計畫" key="2">
                                    {key == 2 ? <Form {...formItem} className="tab-content" >
                                        <Row>
                                            <Col xs={24} lg={12} md={24} sm={24}>
                                                <Row>
                                                    <Form.Item label="考慮車型" hasFeedback>
                                                        {getFieldDecorator("editModel1", {
                                                            initialValue: tabData.car_model1 ? tabData.car_model1 : undefined
                                                        })(
                                                            <Select
                                                                name="editModel1"
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇考慮車型"
                                                                onChange={this.onChangecar}
                                                                optionFilterProp="children">
                                                                {editModel1.map((item, idex) => (
                                                                    <Option value={item.cbip_name} key={idex}>
                                                                        {item.cbip_name}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>

                                                    <Form.Item label="考慮動力" hasFeedback>
                                                        {getFieldDecorator("editPower", {
                                                            initialValue: tabData.car_powertrain ? tabData.car_powertrain : undefined
                                                        })(
                                                            <Select
                                                                name="editPower"
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇考慮動力"
                                                                onChange={this.onChangePower}
                                                                optionFilterProp="children">
                                                                {getCarDetail.map((item, index) => (
                                                                    // console.log(item)
                                                                    <Option value={item.sc} key={index}>
                                                                        {item.sc}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>
                                                    <Form.Item label="考慮車款" hasFeedback>
                                                        {getFieldDecorator("editVariant", {
                                                            initialValue: tabData.car_variants ? tabData.car_variants : undefined
                                                        })(
                                                            <Select
                                                                name="editVariant"
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇考慮車款"
                                                                optionFilterProp="children">
                                                                {getEditPower.map((item, index) => (
                                                                    <Option value={item.sc} key={index}>
                                                                        {item.sc}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>

                                                    <Form.Item label="比較競品-品牌" hasFeedback>
                                                        {getFieldDecorator("editCompetition", {
                                                            initialValue: tabData.car_competition ? tabData.car_competition : undefined
                                                        })(
                                                            <Select setFieldsValue={undefined}
                                                                name="editCompetition"
                                                                showSearch
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇訊息來源"
                                                                optionFilterProp="children">
                                                                {editCompetition.map((item, index) => (
                                                                    <Option value={item.id} key={index}>
                                                                        {item.sps_name}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>

                                                    <Form.Item
                                                        label="比較競品-車型"
                                                        validateStatus={this.props.validateStatus}>
                                                        {getFieldDecorator("editCompetitionModel", {
                                                            initialValue: tabData.car_competition_model ? tabData.car_competition_model : undefined
                                                        })(
                                                            <Input
                                                                style={{ width: 265 }}
                                                                name="editCompetitionModel"
                                                            />
                                                        )}
                                                    </Form.Item>
                                                </Row>
                                            </Col>
                                            <Col xs={24} lg={12} md={24} sm={24}>
                                                <Row>
                                                    <Form.Item label="喜好車色" hasFeedback>
                                                        {getFieldDecorator("editColor", {
                                                            initialValue: tabData.car_color ? tabData.car_color : undefined
                                                        })(
                                                            <Select
                                                                name="editColor"
                                                                showSearch
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇車色"
                                                                optionFilterProp="children"
                                                            >
                                                                {editColor.map((item, idex) => (
                                                                    <Option value={item.id} key={idex}>
                                                                        {item.sps_name}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>
                                                    <Form.Item label="付款方式" hasFeedback>
                                                        {getFieldDecorator("editPaymentMethod", {
                                                            initialValue: tabData.payment_method ? tabData.payment_method : undefined
                                                        })(
                                                            <Select
                                                                name="editPaymentMethod"
                                                                showSearch
                                                                style={{ width: 265 }}
                                                                placeholder="請選擇"
                                                                optionFilterProp="children"
                                                            >
                                                                {editPaymentMethod.map((item, idex) => (
                                                                    <Option value={item.id} key={idex}>
                                                                        {item.sps_name}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>
                                                </Row>
                                            </Col>
                                        </Row>
                                        <Button type="primary" block onClick={this.purchasePlan}>
                                            儲存
                                     </Button>
                                    </Form> : null}


                                </TabPane>
                                <TabPane tab="接觸活動" key="3">
                                    {key == 3 ? <Form {...formItem} className="tab-content">
                                        <Row>
                                            <Col xs={24} lg={12} md={24} sm={24}>
                                                <Row>
                                                    <Form.Item>
                                                        <Checkbox.Group
                                                            style={{ width: "100%" }}
                                                            onChange={this.onCheckbox}>
                                                            <Row>
                                                                <Col span={8}>
                                                                    <Checkbox value="M">預算</Checkbox>
                                                                </Col>
                                                                <Col span={8}>
                                                                    <Checkbox value="T">時間</Checkbox>
                                                                </Col>
                                                                <Col span={8}>
                                                                    <Checkbox value="P">偏好度</Checkbox>
                                                                </Col>
                                                            </Row>
                                                        </Checkbox.Group>
                                                    </Form.Item>
                                                    <Form.Item label="本次接觸日期">
                                                        {getFieldDecorator("trackContactDate", {
                                                            ...config,
                                                            initialValue: moment(new Date(), dateFormat)
                                                        })(<DatePicker style={{ width: 300 }} />)}
                                                    </Form.Item>
                                                    <Form.Item label="本次接觸地點" hasFeedback>
                                                        {getFieldDecorator("trackContactSite", {
                                                            rules: [
                                                                { required: true, message: "請選擇本次接觸地點!" }
                                                            ]
                                                        })(
                                                            <Select
                                                                name="trackContactSite"
                                                                showSearch
                                                                style={{ width: 300 }}
                                                                placeholder="請選擇"
                                                                optionFilterProp="children">
                                                                {trackContactSite.map((item, idex) => (
                                                                    <Option value={item.id} key={idex}>
                                                                        {item.sps_name}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>
                                                    <Form.Item label="本次接觸事項" hasFeedback>
                                                        {getFieldDecorator("trackContactType", {
                                                            rules: [
                                                                {
                                                                    required: true,
                                                                    message: "請選擇本次接觸事項!"
                                                                }
                                                            ]
                                                        })(
                                                            <Select
                                                                name="trackContactType"
                                                                style={{ width: 300 }}
                                                                placeholder="請選擇"
                                                                optionFilterProp="children"
                                                                onChange={this.ChangeType}
                                                            >
                                                                {trackContactType.map((item, idex) => (
                                                                    <Option value={item.id} key={idex}>
                                                                        {item.sps_name}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>

                                                    {TypeValue === '002' ? <Form.Item label="試車車型" hasFeedback>
                                                        {getFieldDecorator("trackTestModel", {
                                                            rules: [
                                                                { required: true, message: "請選擇試車車型!" }
                                                            ]
                                                        })(
                                                            <Select
                                                                name="trackTestModel"
                                                                showSearch
                                                                style={{ width: 300 }}
                                                                placeholder="請選擇"
                                                                optionFilterProp="children">
                                                                {getCarModel.map((item, idex) => (

                                                                    <Option value={item.code} key={idex}>
                                                                        {item.model}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item> : null}

                                                    {TypeValue === "002" ? <Form.Item label="試車人生日" hasFeedback>
                                                        {getFieldDecorator("trackTestBirthday")(
                                                            <DatePicker
                                                                style={{ width: 300 }}
                                                                placeholder="請選擇生日"
                                                            />
                                                        )}
                                                    </Form.Item> : null}

                                                    {TypeValue === "002" ? <Form.Item label="試車人地址" hasFeedback>
                                                        {getFieldDecorator("trackTestAddress")(
                                                            <Input
                                                                style={{ width: 300 }}
                                                                name="trackTestAddress"
                                                            />
                                                        )}
                                                    </Form.Item> : null}

                                                    {TypeValue === "002" ? <Form.Item label="試車人電話" hasFeedback>
                                                        {getFieldDecorator("trackTestPhone", {
                                                            rules: [
                                                                { required: true, message: "請輸入試車人電話!" }
                                                            ]
                                                        })(
                                                            <Input
                                                                style={{ width: 300 }}
                                                                name="trackTestPhone"
                                                            />
                                                        )}
                                                    </Form.Item> : null}

                                                </Row>
                                            </Col>
                                            <Col xs={24} lg={12} md={24} sm={24}>
                                                <Row>
                                                    <Form.Item>
                                                        <Row>
                                                            <Col span={12}>購車熱度</Col>
                                                            {getFieldDecorator("state", {})(
                                                                <Col span={12} id="state">
                                                                    {text}
                                                                </Col>
                                                            )}
                                                        </Row>
                                                    </Form.Item>
                                                    <Form.Item label="下次接觸日期">
                                                        {getFieldDecorator("trackNextContactDate", {
                                                            ...config,
                                                            initialValue: moment(nextDate, dateFormat)
                                                        })(<DatePicker style={{ width: 300 }} />)}
                                                    </Form.Item>
                                                    <Form.Item label="下次接觸事項" hasFeedback>
                                                        {getFieldDecorator("trackNextContactPoint", {
                                                            rules: [
                                                                { required: true, message: "請選擇下次接觸事項!" }
                                                            ]
                                                        })(
                                                            <Select
                                                                name="trackNextContactPoint"
                                                                style={{ width: 300 }}
                                                                placeholder="請選擇訊息來源"
                                                                optionFilterProp="children">
                                                                {nextContactType.map((item, index) => (
                                                                    <Option value={item.id} key={index}>
                                                                        {item.sps_name}
                                                                    </Option>
                                                                ))}
                                                            </Select>
                                                        )}
                                                    </Form.Item>
                                                    <Form.Item label="備註">
                                                        {getFieldDecorator("trackRemarks", {})(
                                                            <TextArea rows={4} />
                                                        )}
                                                    </Form.Item>
                                                </Row>
                                            </Col>
                                            <Col
                                                xs={24}
                                                lg={24}
                                                md={24}
                                                sm={24}
                                                className="text-center">
                                                {getFieldDecorator("isGoogle", {})(
                                                    <Checkbox.Group
                                                        style={{ width: "100%" }}
                                                        onChange={this.GoogleCheck}
                                                    >
                                                        <Row className="my-3">
                                                            <Col span={24}>
                                                                <Checkbox value="checked">
                                                                    新增至Google日曆
                                                         </Checkbox>
                                                            </Col>
                                                        </Row>
                                                    </Checkbox.Group>
                                                )}
                                            </Col>
                                        </Row>
                                        <Button type="primary" block onClick={this.trackSave}>
                                            儲存
                                        </Button>
                                        <Col xs={24}
                                            lg={24}
                                            md={24}
                                            sm={24}>
                                            <Collapse className="mt-3"
                                                bordered={false}
                                                expandIcon={({ isActive }) => <Icon type={isActive ? "zoom-out" : "zoom-in"} />}>
                                                {ListDetail.map((item, index) => (
                                                    <Panel header={item.contact_date} key={index} style={customPanelStyle}>
                                                        <Card title="追蹤紀錄">
                                                            <Row>
                                                                <Col className="text-secondary"
                                                                    xs={7}
                                                                    lg={7}
                                                                    md={7}
                                                                    sm={7}>
                                                                    <p>購車熱度：</p>
                                                                    <p>本次接觸日期：</p>
                                                                    <p>本次接觸地點：</p>
                                                                    <p>本次接觸事項：</p>
                                                                </Col>

                                                                <Col className="text-dark"
                                                                    xs={5}
                                                                    lg={5}
                                                                    md={5}
                                                                    sm={5}>
                                                                    <p>{item.state}</p>
                                                                    <p>{item.contact_date}</p>
                                                                    <p>{item.contact_site}</p>
                                                                    <p>{item.contact_type}</p>
                                                                </Col>

                                                                <Col className="text-secondary"
                                                                    xs={7}
                                                                    lg={7}
                                                                    md={7}
                                                                    sm={7}>
                                                                    <p>下次接觸日期：</p>
                                                                    <p>下次接觸事項：</p>
                                                                    <p>備註：</p>
                                                                </Col>

                                                                <Col className="text-dark"
                                                                    xs={5}
                                                                    lg={5}
                                                                    md={5}
                                                                    sm={5}>
                                                                    <p>{item.next_contact_date}</p>
                                                                    <p>{item.next_contact_point}</p>
                                                                    <p>{item.remark}</p>
                                                                </Col>
                                                            </Row>
                                                        </Card>
                                                    </Panel>
                                                ))}
                                            </Collapse>
                                        </Col>
                                    </Form> : null}
                                </TabPane>

                                <TabPane tab="銷售結果" key="4">
                                    {key == 4 ? <div style={{ padding: '15px' }}>
                                       <TabSalesForm/>
                                    </div> : null}
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </Row>
            </>
        );
    }
}

const RelatedForm = Form.create()(Edit);
export default RelatedForm;
