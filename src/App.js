import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import MainRouter from './component/MainRouter';
import List from './component/List';
import Edit from './component/Edit';
import StockList from './component/StockList';
import Login from './views/Login';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import './scss/App.scss';
import { Row, Col } from 'antd';
import Sidebar from './component/Sidebar';

function App() {
  return (
  
    <div className="container">
      <BrowserRouter>
        <Row type="flex">
        <Col span={5}>
            <Sidebar />
          </Col>
        <Col span={19}>
            <Route>
            <Route exact path="/" component={Login}/>
            <Route path="/SMT/Front/Index/Index.html" component={MainRouter}/>
            <Route path="/SMT/Front/PotentialClient/List.html" component={List}/>
            <Route path="/SMT/Front/PotentialClient/Edit.html" component={Edit}/>
            {/* <Route path="/SMT/Front/PotentialClientQR/QRCode.html" /> */}
            <Route path="/SMT/Front/PotentialClient/StockList.html" component={StockList}/>
          </Route>
        </Col>
        </Row>
        </BrowserRouter>
    </div>
   
  );
}


export default App;
